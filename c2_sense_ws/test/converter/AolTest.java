
package converter;



import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Assert;
import org.junit.Test;

import util.C2SDIUtils;
import util.DocumentFormat;





public class AolTest
{

	@Test
	public void testConvertAolToEdxlRmRqr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.EDXL_RM_RQR_XML.format( ) );
		assertNotNull( "Transformation aol to edxl.rm.rqr should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.rqr.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.EDXL_RM_RQR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMRQR transformation should produce a valid EDXLRMRQR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmRqrToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_RQR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.rm.rqr to aol should be supported", transformer );

		String edxlrmrqr;
		try
		{
			edxlrmrqr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.rqr.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrqr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMRQR transformation should produce a valid EDXLRMRQR", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolToEdxlRmRRR( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.EDXL_RM_RRR_XML.format( ) );
		assertNotNull( "Transformation aol to edxl.rm.rrr should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.rrr.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.EDXL_RM_RRR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMRRR transformation should produce a valid EDXLRMRRR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmRrrToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_RRR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.rm.rrr to aol should be supported", transformer );

		String edxlrmrrr;
		try
		{
			edxlrmrrr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.rrr.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrrr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMRRR transformation should produce a valid EDXLRMRRR", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolToEdxlRmAr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.EDXL_RM_AR_XML.format( ) );
		assertNotNull( "Transformation aol to edxl.rm.ar should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.ar.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.EDXL_RM_AR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMAR transformation should produce a valid EDXLRMAR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmArToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_AR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.rm.ar to aol should be supported", transformer );

		String edxlrmrqr;
		try
		{
			edxlrmrqr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.ar.xml" ), StandardCharsets.UTF_8.name( ) );

			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrqr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMAR transformation should produce a valid EDXLRMAR", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolToEdxlRmCr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.EDXL_RM_CR_XML.format( ) );
		assertNotNull( "Transformation aol to edxl.rm.cr should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.cr.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.EDXL_RM_CR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMCR transformation should produce a valid EDXLRMCR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmCrToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_CR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.rm.cr to aol should be supported", transformer );

		String edxlrmrqr;
		try
		{
			edxlrmrqr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.cr.xml" ), StandardCharsets.UTF_8.name( ) );

			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrqr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "EDXLRMCR TO AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolToSitrepFo( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.SITREP_FO_XML.format( ) );
		assertNotNull( "Transformation aol to edxl.rm.cr should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.fo.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.SITREP_FO_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO SITREPFO transformation should produce a valid SITREPFO", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertSitrepFoToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.SITREP_FO_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation sitrep.fo to aol should be supported", transformer );

		String sitrepfo;
		try
		{
			sitrepfo = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/sitrep.fo.xml" ), StandardCharsets.UTF_8.name( ) );

			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( sitrepfo, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "EDXLRMCR TO AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolToSitrepMr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.SITREP_MR_XML.format( ) );
		assertNotNull( "Transformation aol to sitrep.mr should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.mr.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.SITREP_MR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO SITREPSI transformation should produce a valid SITREPMR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertSitrepMrToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.SITREP_MR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation sitrep.si to aol should be supported", transformer );

		String sitrepfo;
		try
		{
			sitrepfo = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/sitrep.mr.xml" ), StandardCharsets.UTF_8.name( ) );

			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( sitrepfo, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "SITREPSI TO AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}
	
	@Test
	public void testConvertCapSitrepMrToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.SITREP_MR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation sitrep.si to aol should be supported", transformer );

		String sitrepfo;
		try
		{
			sitrepfo = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/sitrep.cap.mr.xml" ), StandardCharsets.UTF_8.name( ) );

			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( sitrepfo, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "SITREPSI TO AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}
	
	public void testConvertAolToSitrepSi( )
	{
		assertTrue( false );
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.SITREP_SI_XML.format( ) );
		assertNotNull( "Transformation aol to sitrep.si should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.si.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.SITREP_SI_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO SITREPSI transformation should produce a valid SITREPSI", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	public void testConvertSitrepSiToAol( )
	{
		assertTrue( false );
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.SITREP_SI_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation sitrep.si to aol should be supported", transformer );

		String sitrepfo;
		try
		{
			sitrepfo = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/sitrep.si.xml" ), StandardCharsets.UTF_8.name( ) );

			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( sitrepfo, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "SITREPSI TO AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolAckToEdxlMpAck( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_ACK_JSON.format( ) + "_" + DocumentFormat.EDXL_MP_ACK_XML.format( ) );
		assertNotNull( "Transformation aol.ack to edxl.mp.ack should be supported", transformer );

		String aolAckString;
		try
		{
			aolAckString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.ack.json" ), StandardCharsets.UTF_8.name( ) );
			String aolAckJsonToXml = C2SDIUtils.convertAolAckJsonToXml( aolAckString );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolAckJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/edxl.de.xml.xsd", streamSource );
			assertTrue( "EDXLRMPACK 2 AOL transformation should produce a valid AOL", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlMpAckToAolAck( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_MP_ACK_XML.format( ) + "_" + DocumentFormat.AOL_ACK_JSON.format( ) );
		assertNotNull( "Transformation edxl.mp.ack to aol ack should be supported", transformer );

		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.mp.ack.xml" ), transformer );
		StreamSource streamSource;
		try
		{
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/aol.ack.json.xsd", streamSource );
			assertTrue( "EDXLRMPACK 2 AOL transformation should produce a valid AOL", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolToEdxlMp( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.EDXL_MP_XML.format( ) );
		assertNotNull( "Transformation aol to edxl.mp should be supported", transformer );
		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.mp.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.EDXL_MP_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLMP transformation should produce a valid EDXLMP", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlMpToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_MP_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.mp to aolshould be supported", transformer );
		try
		{
			StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.mp.xml" ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "EDXLMP TO AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}
	
	
	@Test
	public void testConvertAolToCap( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.CAP_XML.format( ) );
		assertNotNull( "Transformation aol to cap should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO CAP transformation should produce a valid CAP", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}
	

	@Test
	public void testConvertEncodedHtml( )
	{
		String htmlEncoded;
		try
		{
			htmlEncoded = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/encoded_html" ), StandardCharsets.UTF_8.name( ) );
			String htmlEscaped = StringEscapeUtils.unescapeHtml4( htmlEncoded );
			System.out.println( htmlEncoded );
			System.out.println( htmlEscaped );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolJsonHtmlToAolXml( )
	{
		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			String convertAolAckXmlToJson = C2SDIUtils.convertAolXmlToJson( aolJsonToXml );
			System.out.println( aolJsonToXml );
			System.out.println( convertAolAckXmlToJson );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolJsonHtmlTockenzier( )
	{
		String aolString = "== TypeStructure == Volunteer. == Description == Please allocate volunteers to observe roads’ statuses around Lucera. == Quantity == 9";
		Assert.assertTrue( C2SDIUtils.getTockensFromAolBody( aolString ).length == 3 );

		aolString = "== EventType == Alert to population. == Description == Alot of rains has fallen on the mountains. Dont stay in the underground constructions and avoid wandering on the river shore.";
		Assert.assertTrue( C2SDIUtils.getTockensFromAolBody( aolString ).length == 2 );

		aolString = "== TypeStructure == Volunteer. == Description == We need volunteers to observe roads� statuses around Lucera. == Quantity == ";
		Assert.assertTrue( C2SDIUtils.getTockensFromAolBody( aolString ).length == 2 );

		aolString = "== ReportPurpose == Information about recruitment of volunteers. == SituationSummary == Volunteers has been requested for helping identification of cut roads around Lucera. 9 answered to the request and are heading to destination.";
		Assert.assertTrue( C2SDIUtils.getTockensFromAolBody( aolString ).length == 2 );

		aolString = "== ReportPurpose == Information about situation evolution concerning river Carapelle == ImmediateNeeds == == ObservationText == Observers report passing by 7m at observed point. Shores are underwater. Civilian houses are still dry, but ceiling probably underwater.";
		Assert.assertTrue( C2SDIUtils.getTockensFromAolBody( aolString ).length == 2 );
	}

	@Test
	public void testConvertAolJsonHtmlTockenzierMapVersion( )
	{
		String aolString = "== TypeStructure == Volunteer. == Description == Please allocate volunteers to observe roads’ statuses around Lucera. == Quantity == 9";
		Map < String, String > tockensMapFromAolBody = C2SDIUtils.getTockensMapFromAolBody( aolString );

		Assert.assertTrue( tockensMapFromAolBody.get( " TypeStructure " ).equals( "Volunteer." ) );
		Assert.assertTrue( tockensMapFromAolBody.get( " Description " ).equals( "Please allocate volunteers to observe roads’ statuses around Lucera." ) );
		Assert.assertTrue( tockensMapFromAolBody.get( " Quantity " ).equals( "9" ) );

		aolString = "== EventType == Alert to population. == Description == Alot of rains has fallen on the mountains. Dont stay in the underground constructions and avoid wandering on the river shore.";
		tockensMapFromAolBody = C2SDIUtils.getTockensMapFromAolBody( aolString );

		Assert.assertTrue( tockensMapFromAolBody.get( " EventType " ).equals( "Alert to population." ) );
		Assert.assertTrue( tockensMapFromAolBody.get( " Description " ).equals( "Alot of rains has fallen on the mountains. Dont stay in the underground constructions and avoid wandering on the river shore." ) );

		aolString = "== TypeStructure == Volunteer. == Description == We need volunteers to observe roads� statuses around Lucera. == Quantity ==";
		tockensMapFromAolBody = C2SDIUtils.getTockensMapFromAolBody( aolString );
		Assert.assertTrue( tockensMapFromAolBody.get( " TypeStructure " ).equals( "Volunteer." ) );
		Assert.assertTrue( tockensMapFromAolBody.get( " Description " ).equals( "We need volunteers to observe roads� statuses around Lucera." ) );
		Assert.assertTrue( tockensMapFromAolBody.get( " Quantity " ).equals( " " ) );

		aolString = "== ReportPurpose == Information about recruitment of volunteers. == SituationSummary == Volunteers has been requested for helping identification of cut roads around Lucera. 9 answered to the request and are heading to destination.";
		tockensMapFromAolBody = C2SDIUtils.getTockensMapFromAolBody( aolString );
		Assert.assertTrue( tockensMapFromAolBody.get( " ReportPurpose " ).equals( "Information about recruitment of volunteers." ) );
		Assert.assertTrue( tockensMapFromAolBody.get( " SituationSummary " ).equals(
			"Volunteers has been requested for helping identification of cut roads around Lucera. 9 answered to the request and are heading to destination." ) );

		aolString = "== ReportPurpose == Information about situation evolution concerning river Carapelle == ImmediateNeeds == == ObservationText == Observers report passing by 7m at observed point. Shores are underwater. Civilian houses are still dry, but ceiling probably underwater.";
		tockensMapFromAolBody = C2SDIUtils.getTockensMapFromAolBody( aolString );
		Assert.assertTrue( tockensMapFromAolBody.get( " ReportPurpose " ).equals( "Information about situation evolution concerning river Carapelle" ) );
		Assert.assertTrue( tockensMapFromAolBody.get( " ImmediateNeeds " ).equals( " " ) );
		Assert.assertTrue( tockensMapFromAolBody.get( " ObservationText " ).equals(
			"Observers report passing by 7m at observed point. Shores are underwater. Civilian houses are still dry, but ceiling probably underwater." ) );
	}
}
