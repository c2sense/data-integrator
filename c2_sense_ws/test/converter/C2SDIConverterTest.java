
package converter;



import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import generated.registry.TranslatableFormatsRegistry;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import util.C2SDIUtils;
import util.DocumentFormat;
import util.JaxbMapper;





public class C2SDIConverterTest
{
	@Test
	public void testSupportedSourceFormat( )
	{
		assertTrue( DocumentFormat.AOL_JSON.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.AOL_JSON.format( ) ) );
		assertTrue( DocumentFormat.CAP_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.CAP_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_MP_ACK_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.EDXL_MP_ACK_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_MP_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.EDXL_MP_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_RM_AR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.EDXL_RM_AR_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_RM_CR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.EDXL_RM_CR_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_RM_RQR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.EDXL_RM_RQR_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_RM_RRR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.EDXL_RM_RRR_XML.format( ) ) );
		assertTrue( DocumentFormat.IPGW_JSON.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.IPGW_JSON.format( ) ) );
		assertTrue( DocumentFormat.OGC_OM_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.OGC_OM_XML.format( ) ) );
		assertTrue( DocumentFormat.SITREP_FO_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.SITREP_FO_XML.format( ) ) );
		assertTrue( DocumentFormat.SITREP_MR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.SITREP_MR_XML.format( ) ) );
		assertTrue( DocumentFormat.SITREP_SI_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.SITREP_SI_XML.format( ) ) );
		assertTrue( DocumentFormat.H2H_XML.format( ) + " should be supported.", C2SDIConverter.instance.isSourceFormatSupported( DocumentFormat.H2H_XML.format( ) ) );

		assertNotNull( DocumentFormat.CAP_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_MP_ACK_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_MP_ACK_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_MP_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_MP_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_RM_AR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_RM_AR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_RM_CR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_RM_CR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_RM_RQR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_RM_RQR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_RM_RRR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_RM_RRR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.IPGW_JSON.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.IPGW_JSON.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.OGC_OM_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.OGC_OM_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.SITREP_FO_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.SITREP_FO_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.SITREP_MR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.SITREP_MR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.SITREP_SI_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.SITREP_SI_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.H2H_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.H2H_XML.format( ) + ".xsd" ) );

	}

	@Test
	public void testSupportedDestinationFormat( )
	{
		assertTrue( DocumentFormat.CAP_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.CAP_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_MP_ACK_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.EDXL_MP_ACK_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_MP_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.EDXL_MP_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_RM_AR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.EDXL_RM_AR_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_RM_RQR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.EDXL_RM_RQR_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_RM_CR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.EDXL_RM_CR_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_RM_RRR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.EDXL_RM_RRR_XML.format( ) ) );
		assertTrue( DocumentFormat.SITREP_FO_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.SITREP_FO_XML.format( ) ) );
		assertTrue( DocumentFormat.SITREP_MR_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.SITREP_MR_XML.format( ) ) );
		assertTrue( DocumentFormat.SITREP_SI_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.SITREP_SI_XML.format( ) ) );
		assertTrue( DocumentFormat.DE_H2H_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.DE_H2H_XML.format( ) ) );
		assertTrue( DocumentFormat.EDXL_DE_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.EDXL_DE_XML.format( ) ) );
		assertTrue( DocumentFormat.H2H_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.H2H_XML.format( ) ) );
		assertTrue( DocumentFormat.AOL_JSON.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.AOL_JSON.format( ) ) );
		assertTrue( DocumentFormat.OGC_OM_XML.format( ) + " should be supported.", C2SDIConverter.instance.isDestinationFormatSupported( DocumentFormat.OGC_OM_XML.format( ) ) );

		assertNotNull( DocumentFormat.CAP_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_MP_ACK_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_MP_ACK_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_MP_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_MP_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_RM_AR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_RM_AR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_RM_RQR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_RM_RQR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_RM_CR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_RM_CR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_RM_RRR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_RM_RRR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.SITREP_FO_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.SITREP_FO_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.SITREP_MR_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.SITREP_MR_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.SITREP_SI_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.SITREP_SI_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.DE_H2H_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.DE_H2H_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.EDXL_DE_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.EDXL_DE_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.H2H_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.H2H_XML.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.AOL_JSON.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd" ) );
		assertNotNull( DocumentFormat.OGC_OM_XML.format( ) + " should be supported.", C2SDIConverter.instance.getUrlStreamFromResource( "schemas/" + DocumentFormat.OGC_OM_XML.format( ) + ".xsd" ) );
	}

	@Test
	public void testSupportedFormatFileGeneration( )
	{
		TranslatableFormatsRegistry supportedFormatsForResponse = C2SDIUtils.getSupportedFormatsForResponse( );

		supportedFormatsForResponse.getFormat( ).addAll(
			Arrays.asList(
				C2SDIUtils.createSupportedFormat( DocumentFormat.IPGW_JSON, "IP GATEWAY FORMAT",
					DocumentFormat.OGC_OM_XML ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.OGC_OM_XML, "Open Geospatial Consortium Observation and Measurement",
					DocumentFormat.OGC_OM_XML ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.AOL_JSON, "ActOnline Json",
					DocumentFormat.CAP_XML,
					DocumentFormat.EDXL_MP_XML,
					DocumentFormat.EDXL_MP_ACK_XML,
					DocumentFormat.EDXL_RM_AR_XML,
					DocumentFormat.EDXL_RM_RQR_XML,
					DocumentFormat.EDXL_RM_CR_XML,
					DocumentFormat.EDXL_RM_RRR_XML,
					DocumentFormat.SITREP_FO_XML,
					DocumentFormat.SITREP_MR_XML,
					DocumentFormat.SITREP_SI_XML
					),
				C2SDIUtils.createSupportedFormat( DocumentFormat.EDXL_RM_RRR_XML, "EDXL RM Response to Request Rresource",
					DocumentFormat.AOL_JSON ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.EDXL_RM_CR_XML, "EDXL RM Commit Resource",
					DocumentFormat.AOL_JSON ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.CAP_XML, "CAP alert format",
					DocumentFormat.DE_H2H_XML,
					DocumentFormat.H2H_XML,
					DocumentFormat.SITREP_MR_XML,
					DocumentFormat.EDXL_DE_ACK_XML,
					DocumentFormat.EDXL_MP_ACK_XML,
					DocumentFormat.EDXL_RM_CR_XML,
					DocumentFormat.EDXL_RM_RRR_XML,
					DocumentFormat.SITREP_MR_XML ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.EDXL_RM_AR_XML, "EDXL RM CommitResource",
					DocumentFormat.CAP_XML, DocumentFormat.AOL_JSON ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.EDXL_RM_RQR_XML, "EDXL RM Request Resource ",
					DocumentFormat.CAP_XML,
					DocumentFormat.AOL_JSON,
					DocumentFormat.H2H_XML ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.EDXL_MP_XML, "EDXL Mission Plan",
					DocumentFormat.CAP_XML,
					DocumentFormat.AOL_JSON,
					DocumentFormat.MSG_SMS_SEND_TXT ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.EDXL_MP_ACK_XML, "EDXL Mission Plan ACK",
					DocumentFormat.AOL_ACK_JSON ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.AOL_ACK_JSON, "AOL ACK",
					DocumentFormat.EDXL_MP_ACK_XML ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.SITREP_MR_XML, "EDXL SitRep Management reposrt",
					DocumentFormat.CAP_XML,
					DocumentFormat.AOL_JSON ),

				C2SDIUtils.createSupportedFormat( DocumentFormat.SITREP_FO_XML, "EDXL SitRep Field Observation",
					DocumentFormat.AOL_JSON ),

				C2SDIUtils.createSupportedFormat( DocumentFormat.SITREP_SI_XML, "EDXL SitRep Situation Information",
					DocumentFormat.AOL_JSON ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.H2H_XML, "Human to Human",
						DocumentFormat.AOL_JSON ),

				C2SDIUtils.createSupportedFormat( DocumentFormat.MSG_SMS_RECEIVED_TXT, "TRBONET MSG SMS RECEIVED",
					DocumentFormat.SITREP_FO_XML ),
				C2SDIUtils.createSupportedFormat( DocumentFormat.MSG_GPS_CALL_RECEIVED_TXT, "TRBONET MSG GPS CALL RECEIVED",
					DocumentFormat.OGC_OM_XML )
					
				) );
		OutputStream marshal = JaxbMapper.marshal( supportedFormatsForResponse );

		System.out.println( marshal.toString( ) );

		StreamSource streamSource = new StreamSource( IOUtils.toInputStream( marshal.toString( ) ) );

		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/c2sdi/TranslatableFormatRegistry.xsd", streamSource );
		assertTrue( "Should be a valid TranslatableFormatsRegistry XML", isInputValid );
	}

	@Test
	public void testConvertH2HToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.H2H_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.rm.rqr to h2h should be supported", transformer );

		String h2h;
		try
		{
			h2h = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/h2h.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( h2h, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO H2H transformation should produce a valid H2H", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}
	
	@Test
	public void testConvertEdxlRmRqrToH2H( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_RQR_XML.format( ) + "_" + DocumentFormat.H2H_XML.format( ) );
		assertNotNull( "Transformation edxl.rm.rqr to h2h should be supported", transformer );

		String edxlrmrqr;
		try
		{
			edxlrmrqr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.rqr.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrqr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.H2H_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO H2H transformation should produce a valid H2H", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertSitRepMrToH2H( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.SITREP_MR_XML.format( ) + "_" + DocumentFormat.H2H_XML.format( ) );
		assertNotNull( "Transformation sitrepmr to h2h should be supported", transformer );

		String edxlrmrqr;
		try
		{
			edxlrmrqr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/sitrep.mr.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrqr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.H2H_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO H2H transformation should produce a valid H2H", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}	
	
	@Test
	public void testSupportedTransformation( )
	{
		List < String > supportedTranslation = C2SDIUtils.getSupportedTranslation( );
		assertNotNull( "At least one translation should be supported", supportedTranslation );

		for ( String string : supportedTranslation )
		{
			assertNotNull( string + " transformation should be supported", C2SDIConverter.instance.getTransformer( string ) );
		}
	}

	@Test
	public void testUnsupportedTransformation( )
	{
		assertNull( "Cap to OGC.OM transformation should not be supported", C2SDIConverter.instance.getTransformer( "Cap.xml_ogc.om.xml" ) );
	}

	@Test
	public void testValidateCapXML( )
	{
		StreamSource streamSource = new StreamSource( C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.xml" ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/cap.xml.xsd", streamSource );
		assertTrue( "Cap alert bundled in application should be valid", isInputValid );
	}

	@Test
	public void testValidateEDXLRMCRXML( )
	{
		StreamSource streamSource = new StreamSource( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.cr.xml" ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/edxl.rm.cr.xml.xsd", streamSource );
		assertTrue( "EDXLRMCR bundled in application should be valid", isInputValid );
	}

	@Test
	public void testValidateEDXLRMRRRXML( )
	{
		StreamSource streamSource = new StreamSource( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.rrr.xml" ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/edxl.rm.rrr.xml.xsd", streamSource );
		assertTrue( "EDXLRMCR bundled in application should be valid", isInputValid );
	}

	@Test
	public void testConvertAolJsonToAolXml( )
	{
		String convertAolJsonToXml = null;

		try
		{
			convertAolJsonToXml = C2SDIUtils.convertAolJsonToXml( IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.json" ) ), true );
		}
		catch ( IOException e )
		{
		}

		StreamSource streamSource = new StreamSource( IOUtils.toInputStream( convertAolJsonToXml ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/aol.json.xsd", streamSource );
		assertTrue( "Aol json alert bundled in application should be valid", isInputValid );
	}

	@Test
	public void testConvertIpgwJsonToIpgwXml( )
	{
		String convertIpgwJsonToXml = null;

		try
		{
			convertIpgwJsonToXml = C2SDIUtils.convertIpgwJsonToXml( IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/ipgw.json" ) ) );
		}
		catch ( IOException e )
		{

		}

		StreamSource streamSource = new StreamSource( IOUtils.toInputStream( convertIpgwJsonToXml ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/ipgw.json.xsd", streamSource );
		assertTrue( "Ipgw json alert bundled in application should be valid", isInputValid );
	}

	@Test
	public void testConvertIpgwJsonToOgcOmXml( )
	{
		String convertIpgwJsonToXml = null;

		try
		{
			convertIpgwJsonToXml = C2SDIUtils.convertIpgwJsonToXml( IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/ipgw.json" ) ) );
		}
		catch ( IOException e )
		{
		}

		Transformer transformer = C2SDIConverter.instance.getTransformer( "ipgw.json_ogc.om.xml" );
		StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( convertIpgwJsonToXml ), transformer );

		StreamSource streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ) ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/ogc.om.xml.xsd", streamSource );
		assertTrue( "IPGW.JSON to OGC.OM.XML transformation should produce a valid OGC.OM.XML", isInputValid );
		System.out.println( conversionWriter.toString( ) );
	}

	@Test
	public void testConvertCapToEdxlMpAck( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( "cap.xml_edxl.mp.ack.xml" );
		assertNotNull( "Transformation cap to edxl.cr.cr should be supported", transformer );

		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.xml" ), transformer );
		StreamSource streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ) ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/edxl.de.xml.xsd", streamSource );
		assertTrue( "CAP to EDXLMPACK transformation should produce a valid EDXLMPACK", isInputValid );
	}

	public void testConvertAolToEdxlRmAr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( "aol.json_edxl.rm.ar.xml" );
		assertNotNull( "Transformation aol to edxl.rm.ar should be supported", transformer );

		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.xml" ), transformer );
		System.out.println( conversionWriter.toString( ) );
		StreamSource streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ) ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/edxl.rm.ar.xml.xsd", streamSource );
		assertTrue( "AOL 2 EDXLRMAR transformation should produce a valid EDXLRMAR", isInputValid );
	}

	public void testConvertEdxlMpToCap( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( "edxl.mp.xml_cap.xml" );
		assertNotNull( "Transformation mp to cap should be supported", transformer );

		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.mp.xml" ), transformer );
		StreamSource streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ) ) );
		Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/cap.xml.xsd", streamSource );
		assertTrue( "EDXLMP to CAP transformation should produce a valid CAP", isInputValid );
	}

	@Test
	public void testCapToAolAddressConversion( )
	{
		final String regex = "\"(.*?)\"";
		final String addressString = "\"1:gmail\" \"2:yahoo\"";

		final Pattern pattern = Pattern.compile( regex );
		final Matcher matcher = pattern.matcher( addressString );

		while ( matcher.find( ) )
		{
			System.out.println( "Full match: " + matcher.group( 0 ) );
			for ( int i = 1; i <= matcher.groupCount( ); i ++ )
			{
				System.out.println( "Group " + i + ": " + matcher.group( i ) );
			}
		}
	}
}
