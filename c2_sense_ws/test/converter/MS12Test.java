
package converter;



import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import util.C2SDIUtils;
import util.DocumentFormat;





public class MS12Test
{
	@Test
	public void testConvertAolToEdxlRmRqr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.EDXL_RM_RQR_XML.format( ) );
		assertNotNull( "Transformation aol to edxl.rm.rqr should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.rqr.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.EDXL_RM_RQR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMRQR transformation should produce a valid EDXLRMRQR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmRqrToCap( )
	{
		assertTrue( false );
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_RQR_XML.format( ) + "_" + DocumentFormat.CAP_XML.format( ) );
		assertNotNull( "Transformation edxl.rm.rqr to cap should be supported", transformer );

		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.rqr.xml" ), transformer );
		System.out.println( conversionWriter.toString( ) );
		StreamSource streamSource;
		try
		{
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/cap.xml.xsd", streamSource );
			assertTrue( "EDXLRMRQR to CAP transformation should produce a valid cap", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertCapRRRToEdxlRmRRR( )
	{
		assertTrue( false );
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.CAP_XML.format( ) + "_" + DocumentFormat.EDXL_RM_RRR_XML.format( ) );
		assertNotNull( "Transformation cap.rrr.xml to edxl.rm.rrr should be supported", transformer );

		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.rrr.xml" ), transformer );
		System.out.println( conversionWriter.toString( ) );
		StreamSource streamSource;
		try
		{
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/edxl.rm.rrr.xml.xsd", streamSource );
			assertTrue( "CAP to EDXLRMRRR transformation should produce a valid EDXLRMRRR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmRrrToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_RRR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.rm.rrr to aol should be supported", transformer );

		String edxlrmrrr;
		try
		{
			edxlrmrrr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.rrr.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrrr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMRRR transformation should produce a valid EDXLRMRRR", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertAolToEdxlRmAr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.AOL_JSON.format( ) + "_" + DocumentFormat.EDXL_RM_AR_XML.format( ) );
		assertNotNull( "Transformation aol to edxl.rm.ar should be supported", transformer );

		String aolString;
		try
		{
			aolString = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.html.ar.json" ), StandardCharsets.UTF_8.name( ) );
			String aolJsonToXml = C2SDIUtils.convertAolJsonToXml( aolString, true );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( aolJsonToXml, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.EDXL_RM_AR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMAR transformation should produce a valid EDXLRMAR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmArToCap( )
	{
		assertTrue( false );
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_AR_XML.format( ) + "_" + DocumentFormat.CAP_XML.format( ) );
		assertNotNull( "Transformation edxl.rm.ar to cap should be supported", transformer );

		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.ar.xml" ), transformer );
		System.out.println( conversionWriter.toString( ) );
		StreamSource streamSource;
		try
		{
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/cap.xml.xsd", streamSource );
			assertTrue( "EDXLRMAR to CAP transformation should produce a valid cap", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertCapCRToEdxlRmCR( )
	{
		assertTrue( false );
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.CAP_XML.format( ) + "_" + DocumentFormat.EDXL_RM_CR_XML.format( ) );
		assertNotNull( "Transformation cap.rrr.xml to edxl.rm.rrr should be supported", transformer );

		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.cr.xml" ), transformer );
		System.out.println( conversionWriter.toString( ) );
		StreamSource streamSource;
		try
		{
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/edxl.rm.cr.xml.xsd", streamSource );
			assertTrue( "CAP to EDXLRMCR transformation should produce a valid EDXLRMCR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmCrToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_CR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.rm.cr to aol should be supported", transformer );

		String edxlrmrqr;
		try
		{
			edxlrmrqr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.cr.xml" ), StandardCharsets.UTF_8.name( ) );

			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrqr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "EDXLRMCR TO AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}
}
