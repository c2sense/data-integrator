
package converter;



import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import util.C2SDIUtils;
import util.DocumentFormat;





public class CapTest
{

	/*
	 * This was useful for MCP
	 */
	@Test
	public void testConvertEdxlDeToCap( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_DE_XML.format( ) + "_" + DocumentFormat.CAP_XML.format( ) );
		assertNotNull( "Transformation edxl.de to cap should be supported", transformer );
		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.de.xml" ), transformer );
		System.out.println( conversionWriter.toString( ) );
		StreamSource streamSource;
		try
		{
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd", streamSource );
			assertTrue( "EDXL.DE 2 CAP transformation should produce a valid CAP", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertCapEmtToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.CAP_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation cap.xml to aol.json should be supported", transformer );
		Boolean isCapValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd", new StreamSource(
			C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.emt.xml" ), "UTF-8" ) );
		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.emt.xml" ), transformer );
		System.out.println( conversionWriter.toString( ) );
		StreamSource streamSource;
		try
		{
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "CAP 2 AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertCapAolToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.CAP_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation cap.xml to aol.json should be supported", transformer );
		Boolean isCapValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd", new StreamSource(
			C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.aol.xml" ), "UTF-8" ) );
		StringWriter conversionWriter = C2SDIConverter.instance.convert( C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.aol.xml" ), transformer );
		System.out.println( conversionWriter.toString( ) );
		StreamSource streamSource;
		try
		{
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "CAP 2 AOL transformation should produce a valid AOL", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmRqrToCap( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_RQR_XML.format( ) + "_" + DocumentFormat.CAP_XML.format( ) );
		assertNotNull( "Transformation edxl.rm.rqr to cap should be supported", transformer );

		String edxlrmrqr;
		try
		{
			edxlrmrqr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.rqr.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrqr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd", streamSource );
			assertTrue( "EDXLRMRQR to CAP transformation should produce a valid CAP", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertCapToEdxlRmRrr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.CAP_XML.format( ) + "_" + DocumentFormat.EDXL_RM_RRR_XML.format( ) );
		assertNotNull( "Transformation cap to edxl.rm.rrr should be supported", transformer );

		String cap;
		try
		{
			cap = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.rrr.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( cap, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.EDXL_RM_RRR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "CAP to EDXLRMRRR transformation should produce a valid EDXLRMRRR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmRrrToAol( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_RRR_XML.format( ) + "_" + DocumentFormat.AOL_JSON.format( ) );
		assertNotNull( "Transformation edxl.rm.rrr to aol should be supported", transformer );

		String edxlrmrrr;
		try
		{
			edxlrmrrr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.rrr.cap.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrrr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.AOL_JSON.format( ) + ".xsd", streamSource );
			assertTrue( "AOL TO EDXLRMRRR transformation should produce a valid EDXLRMRRR", isInputValid );
			System.out.println( C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) ) );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlRmArToCap( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_RM_AR_XML.format( ) + "_" + DocumentFormat.CAP_XML.format( ) );
		assertNotNull( "Transformation edxl.rm.ar to cap should be supported", transformer );

		String edxlrmrqr;
		try
		{
			edxlrmrqr = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.rm.ar.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlrmrqr, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd", streamSource );
			assertTrue( "EDXLRMAR to CAP transformation should produce a valid CAP", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertEdxlMpToCap( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.EDXL_MP_XML.format( ) + "_" + DocumentFormat.CAP_XML.format( ) );
		assertNotNull( "Transformation edxl.mp to cap should be supported", transformer );

		String edxlmp;
		try
		{
			edxlmp = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/edxl.mp.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( edxlmp, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.CAP_XML.format( ) + ".xsd", streamSource );
			assertTrue( "EDXLMP to CAP transformation should produce a valid CAP", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}

	@Test
	public void testConvertCapToSitrepMr( )
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer( DocumentFormat.CAP_XML.format( ) + "_" + DocumentFormat.SITREP_MR_XML.format( ) );
		assertNotNull( "Transformation cap to sitrep.mr should be supported", transformer );

		String cap;
		try
		{
			cap = IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/cap.mr.xml" ), StandardCharsets.UTF_8.name( ) );
			StringWriter conversionWriter = C2SDIConverter.instance.convert( IOUtils.toInputStream( cap, StandardCharsets.UTF_8.name( ) ), transformer );
			StreamSource streamSource;
			System.out.println( conversionWriter.toString( ) );
			streamSource = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ), "UTF-8" ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + DocumentFormat.SITREP_MR_XML.format( ) + ".xsd", streamSource );
			assertTrue( "CAP to EDXLRMRRR transformation should produce a valid EDXLRMRRR", isInputValid );
		}
		catch ( IOException e )
		{
		}
	}
}
