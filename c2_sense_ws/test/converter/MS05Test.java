
package converter;



import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import util.C2SDIUtils;
import util.DocumentFormat;





public class MS05Test
{

	@Test
	public void testConvertMsgSmsReceivedToSitrepFO() {
		Transformer transformer = C2SDIConverter.instance.getTransformer(DocumentFormat.MSG_SMS_RECEIVED_TXT.format() + "_" + DocumentFormat.SITREP_FO_XML.format());
		assertNotNull("Transformation msg.sms.received to sitrep.fo should be supported", transformer);

		InputStream fileContent = C2SDIConverter.instance.getInputStreamFromResource("xml/msg.sms.received.txt");
		String msgSmsReceivedXml = "";
		try {
			msgSmsReceivedXml = C2SDIUtils.convertMsgSmsReceivedTxtToXml(IOUtils.toString(fileContent));
		} catch (IOException e) {
		}
		System.out.println(msgSmsReceivedXml);
		StringWriter conversionWriter = new StringWriter();
		try {
			conversionWriter = C2SDIConverter.instance.convert(IOUtils.toInputStream(msgSmsReceivedXml, StandardCharsets.UTF_8.name()), transformer);
		} catch (IOException e) {
		}
		
		System.out.println(conversionWriter.toString());
		
		StreamSource streamSource;
		try {
			streamSource = new StreamSource(IOUtils.toInputStream(conversionWriter.toString(), "UTF-8"));
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL("schemas/sitrep.fo.xml.xsd", streamSource);
			assertTrue("MSG.SMS.RECEIVED 2 SITREP.FO transformation should produce a valid SITREP.FO", isInputValid);
		} catch (IOException e) {
		}
	}
	
	@Test
	public void testConvertMissionPlanToMsgSmsSend() {
		Transformer transformer = C2SDIConverter.instance.getTransformer(DocumentFormat.EDXL_MP_XML.format() + "_" + DocumentFormat.MSG_SMS_SEND_TXT.format());
		assertNotNull("Transformation edxl.mp to msg.sms.send should be supported", transformer);

		StringWriter conversionWriter = C2SDIConverter.instance.convert(C2SDIConverter.instance.getInputStreamFromResource("xml/edxl.mp.xml"), transformer);
		StreamSource streamSource;
		try {
			streamSource = new StreamSource(IOUtils.toInputStream(conversionWriter.toString(), "UTF-8"));
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL("schemas/msg.sms.send.txt.xsd", streamSource);
			assertTrue("EDXL.MP 2 MSG.SMS.SEND transformation should produce a valid MSG.SMS.SEND", isInputValid);
		} catch (IOException e) {
		}
		
		String fileContent = C2SDIUtils.convertMsgSmsSendXmlToTxt(conversionWriter.toString());
		System.out.println(fileContent);
		assertNotNull("Error during conversion MSG.SMS.SEND.XML to TRBONET format", fileContent);
	}
	
	@Test
	public void testConvertMsgGpsCallReceivedToOgcOm()
	{
		Transformer transformer = C2SDIConverter.instance.getTransformer(DocumentFormat.MSG_GPS_CALL_RECEIVED_TXT.format() + "_" + DocumentFormat.OGC_OM_XML.format());
		assertNotNull("Transformation msg.gps.call.received to ogc.om should be supported", transformer);

		InputStream fileContent = C2SDIConverter.instance.getInputStreamFromResource("xml/msg.gps.call.received.txt");
		String msgGpsCallReceivedXml = "";
		try {
			msgGpsCallReceivedXml = C2SDIUtils.convertMsgGpsCallReceivedToXml(IOUtils.toString(fileContent));
		} catch (IOException e) {
		}
		
		StringWriter conversionWriter = new StringWriter();
		try {
			conversionWriter = C2SDIConverter.instance.convert(IOUtils.toInputStream(msgGpsCallReceivedXml, StandardCharsets.UTF_8.name()), transformer);
		} catch (IOException e) {
		}
		System.out.println(conversionWriter.toString());
		StreamSource streamSource;
		try {
			streamSource = new StreamSource(IOUtils.toInputStream(conversionWriter.toString(), "UTF-8"));
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL("schemas/ogc.om.xml.xsd", streamSource);
			assertTrue("MSG.GPS.CALL.RECEIVED 2 OGC.OM transformation should produce a valid OGC.OM", isInputValid);
		} catch (IOException e) {
		}
	}
}
