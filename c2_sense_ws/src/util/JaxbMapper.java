
package util;



import generated.aol.AOLMessage;
import generated.aolack.AOLACKMessage;
import generated.ipgw.Root;
import generated.registry.TranslatableFormatsRegistry;
import generated.trbonet.MsgGpsCallReceived;
import generated.trbonet.MsgSmsReceived;
import generated.trbonet.MsgSmsSend;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.google.common.base.Throwables;





public class JaxbMapper
{
	/** Heavyweight, thread-safe */
	private static final JAXBContext		jaxbContext;

	private static final XMLInputFactory	xmlInputFactory;

	static
	{
		try
		{
			jaxbContext = JAXBContext.newInstance( AOLMessage.class, AOLACKMessage.class, Root.class, TranslatableFormatsRegistry.class, MsgSmsReceived.class, MsgSmsSend.class, MsgGpsCallReceived.class );
		}
		catch ( JAXBException e )
		{
			throw Throwables.propagate( e );
		}

		xmlInputFactory = XMLInputFactory.newInstance( );
	}

	private JaxbMapper( )
	{
	}

	private static Marshaller getMarshaller( )
	{
		try
		{
			Marshaller marshaller = jaxbContext.createMarshaller( );
			marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );

			marshaller.setEventHandler( new ValidationEventHandler( )
			{
				@Override
				public boolean handleEvent( ValidationEvent event )
				{
					throw new RuntimeException( event.getMessage( ), event.getLinkedException( ) );
				}
			} );

			return marshaller;
		}
		catch ( JAXBException e )
		{
			throw Throwables.propagate( e );
		}
	}

	private static Unmarshaller getUnmarshaller( )
	{
		try
		{
			// 1 - instantiate, make it aware of the required entities
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller( );

			// 2 - configure exception management: default unmarshaller implementations
			// *SWALLOW* (!!!) any xml-validation error (!!!)
			unmarshaller.setEventHandler( new ValidationEventHandler( )
			{
				@Override
				public boolean handleEvent( ValidationEvent event )
				{
					throw new RuntimeException( event.getMessage( ), event.getLinkedException( ) );
				}
			} );

			return unmarshaller;
		}
		catch ( JAXBException e )
		{
			throw Throwables.propagate( e );
		}
	}

	public static < T > T unmarshal( InputStream in, Class < T > clazz )
	{
		XMLStreamReader xsr;

		try
		{
			xsr = xmlInputFactory.createXMLStreamReader( in );
		}
		catch ( XMLStreamException e )
		{
			throw Throwables.propagate( e );
		}

		JAXBElement < T > x;

		try
		{
			x = getUnmarshaller( ).unmarshal( xsr, clazz );
		}
		catch ( JAXBException e )
		{
			throw Throwables.propagate( e );
		}

		return x.getValue( );
	}

	public static < T > OutputStream marshal( T value )
	{
		OutputStream out = new ByteArrayOutputStream( 32 * 1024 );

		try
		{
			getMarshaller( ).marshal( value, out );
		}
		catch ( JAXBException e )
		{
			throw Throwables.propagate( e );
		}

		return out;
	}
}
