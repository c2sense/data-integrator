
package util;



import java.util.HashSet;





public enum DocumentFormat
{
	IPGW_JSON( "ipgw.json" )
	, OGC_OM_XML( "ogc.om.xml" )
	, AOL_JSON( "aol.json" )
	, AOL_ACK_JSON ("aol.ack.json")
	, DE_H2H_XML( "deh2h.xml" )
	, H2H_XML( "h2h.xml" )
	, CAP_XML( "cap.xml" )
	, SITREP_MR_XML( "sitrep.mr.xml" )
	, SITREP_SI_XML( "sitrep.si.xml" )
	, SITREP_FO_XML( "sitrep.fo.xml" )
	, EDXL_RM_RRR_XML( "edxl.rm.rrr.xml" )
	, EDXL_RM_CR_XML( "edxl.rm.cr.xml" )
	, EDXL_RM_RQR_XML( "edxl.rm.rqr.xml" )
	, EDXL_RM_AR_XML( "edxl.rm.ar.xml" )
	, EDXL_MP_ACK_XML( "edxl.mp.ack.xml" )
	, EDXL_DE_XML( "edxl.de.xml" )
	, EDXL_DE_ACK_XML( "edxl.de.ack.xml" )
	, EDXL_MP_XML( "edxl.mp.xml" )
	, MSG_SMS_RECEIVED_TXT( "msg.sms.received.txt" )
	, MSG_SMS_SEND_TXT( "msg.sms.send.txt" )
	, MSG_GPS_CALL_RECEIVED_TXT ( "msg.gps.call.received.txt" );

	private String	format;

	DocumentFormat( String format )
	{
		this.format = format;
	}

	public String format( )
	{
		return this.format;
	}
	
	public static HashSet < String > getSupportedSourceFormat( )
	{
		HashSet < String > values = new HashSet < String >( );

		values.add( DocumentFormat.AOL_JSON.format );
		values.add( DocumentFormat.AOL_ACK_JSON.format );
		values.add( DocumentFormat.CAP_XML.format );
		values.add( DocumentFormat.EDXL_MP_ACK_XML.format );
		values.add( DocumentFormat.EDXL_MP_XML.format );
		values.add( DocumentFormat.EDXL_RM_AR_XML.format );
		values.add( DocumentFormat.EDXL_RM_CR_XML.format );
		values.add( DocumentFormat.EDXL_RM_RQR_XML.format );
		values.add( DocumentFormat.EDXL_RM_RRR_XML.format );
		values.add( DocumentFormat.IPGW_JSON.format );
		values.add( DocumentFormat.OGC_OM_XML.format );
		values.add( DocumentFormat.SITREP_FO_XML.format );
		values.add( DocumentFormat.SITREP_MR_XML.format );
		values.add( DocumentFormat.SITREP_SI_XML.format );
		values.add( DocumentFormat.MSG_SMS_RECEIVED_TXT.format );
		values.add( DocumentFormat.MSG_GPS_CALL_RECEIVED_TXT.format );
		values.add( DocumentFormat.EDXL_DE_XML.format );
		values.add( DocumentFormat.H2H_XML.format );
			
		return values;
	}

	public static HashSet < String > getSupportedDestinationFormat( )
	{
		HashSet < String > values = new HashSet < String >( );

		values.add( DocumentFormat.CAP_XML.format );
		values.add( DocumentFormat.EDXL_MP_ACK_XML.format );
		values.add( DocumentFormat.EDXL_MP_XML.format );
		values.add( DocumentFormat.EDXL_RM_AR_XML.format );
		values.add( DocumentFormat.EDXL_RM_RQR_XML.format );
		values.add( DocumentFormat.EDXL_RM_CR_XML.format );
		values.add( DocumentFormat.EDXL_RM_RRR_XML.format );
		values.add( DocumentFormat.SITREP_FO_XML.format );
		values.add( DocumentFormat.SITREP_MR_XML.format );
		values.add( DocumentFormat.SITREP_SI_XML.format );
		values.add( DocumentFormat.DE_H2H_XML.format );
		values.add( DocumentFormat.EDXL_DE_XML.format );
		values.add( DocumentFormat.H2H_XML.format );
		values.add( DocumentFormat.AOL_JSON.format );
		values.add( DocumentFormat.OGC_OM_XML.format );
		values.add( DocumentFormat.MSG_SMS_SEND_TXT.format );
		
		return values;
	}


	public static HashSet < String > getEnums( )
	{
		HashSet < String > values = new HashSet < String >( );

		for ( DocumentFormat c : DocumentFormat.values( ) )
		{
			values.add( c.format );
		}
		return values;
	}
}
