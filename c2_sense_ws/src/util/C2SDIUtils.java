
package util;



import generated.aol.AOLMessage;
import generated.aolack.AOLACKMessage;
import generated.emt.EmtMessage;
import generated.ipgw.Root;
import generated.registry.ObjectFactory;
import generated.registry.TranslatableFormatsRegistry;
import generated.registry.TranslatableFormatsRegistry.Format;
import generated.trbonet.MsgGpsCallReceived;
import generated.trbonet.MsgSmsReceived;
import generated.trbonet.MsgSmsSend;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;
import com.google.common.collect.Lists;

import converter.C2SDIConverter;





public class C2SDIUtils
{
	public static String			SMS_STATION			= "dummyStation";
	public static String			SMS_OPERATOR		= "dummyOperator";
	public static Long				SMS_SEND_COMMAND	= 32798L;
	public static int				SMS_MESSAGE_LIMIT	= 280;

	public static Set < String >	blackList			= null;
	public static ObjectFactory		registryFactory		= null;
	public static ObjectMapper		mapper				= null;

	public static BigDecimal		divisor				= new BigDecimal( 100000 );

	static
	{
		registryFactory = new ObjectFactory( );
		mapper = new ObjectMapper( );
		Builder < String > builder = ImmutableSet.builder( );
		builder
			.add( "ReportPurpose" )
			.add( "ImmediateNeeds" )
			.add( "ObservationText" )
			.add( "SituationSummary" )
			.add( "TypeStructure" )
			.add( "Description" )
			.add( "Quantity" )
			.add( "EventType" )
			.add( "Description" );
		blackList = builder.build( );
	}

	public static String ZuluDate( )
	{
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss'Z'" );
		TimeZone gmt = TimeZone.getTimeZone( "GMT" );
		sdf.setTimeZone( gmt );
		return sdf.format( new Date( ) );
	}

	public static String UTCDate( )
	{
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssXXX" );
		TimeZone gmt = TimeZone.getTimeZone( "Europe/Berlin" );
		sdf.setTimeZone( gmt );
		return sdf.format( new Date( ) );
	}

	public static String convertDateIntoUTCDate( Date date )
	{
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssXXX" );
		TimeZone gmt = TimeZone.getTimeZone( "Europe/Berlin" );
		sdf.setTimeZone( gmt );
		return sdf.format( date );
	}

	public static String splitString( String text, String separator, int returnIndex )
	{
		String [ ] split = text.split( separator );

		if ( returnIndex >= split.length )
		{
			return "";
		}
		return split[ returnIndex ];
	}

	public static String trimString( String text )
	{
		return text.trim( );
	}

	public static String extractMessageFromEMTCap( String event )
	{
		final String regex = "\\{(?:[^\\{}]|)*\\}";
		final Pattern pattern = Pattern.compile( regex );
		final Matcher matcher = pattern.matcher( event );
		String jsonInEmt = null;

		while ( matcher.find( ) )
		{
			System.out.println( "Full match: " + matcher.group( 0 ) );
			jsonInEmt = matcher.group( 0 );
			for ( int i = 1; i <= matcher.groupCount( ); i ++ )
			{
				System.out.println( "Group " + i + ": " + matcher.group( i ) );
			}
		}

		if ( jsonInEmt == null )
		{
			return event;
		}

		EmtMessage emtMessageObj = null;
		try
		{
			emtMessageObj = mapper.readValue( jsonInEmt, EmtMessage.class );
		}
		catch ( JsonParseException e )
		{
			System.out.println( e );
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		catch ( JsonMappingException e )
		{
			System.out.println( e );
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		catch ( IOException e )
		{
			System.out.println( e );
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		return emtMessageObj.getText( );
	}

	/**
	 * 
	 * Join signaling and execution from mission plan and cut the string to 140 char for TRBONET sms
	 * 
	 * */
	public static String cutMessageLength( String signaling, String execution )
	{
		StringBuilder sb = new StringBuilder( signaling );
		sb.append( " " )
			.append( execution );

		if ( sb.length( ) > SMS_MESSAGE_LIMIT )
		{
			return sb.substring( 0, SMS_MESSAGE_LIMIT );
		}

		return sb.toString( );
	}

	public static TranslatableFormatsRegistry getSupportedFormatsForResponse( )
	{
		return registryFactory.createTranslatableFormatsRegistry( );
	}

	public static ImmutableMap < String, String > getTockensMapFromAolBody( String aolBody )
	{
		com.google.common.collect.ImmutableMap.Builder < String, String > builder = ImmutableMap.builder( );

		StringTokenizer aolMessageTockenizer = new StringTokenizer( aolBody, "==" );
		while ( aolMessageTockenizer.hasMoreElements( ) )
		{
			String key = aolMessageTockenizer.nextElement( ).toString( );
			try
			{
				String value = aolMessageTockenizer.nextElement( ).toString( );
				builder.put( key, StringUtils.isBlank( value ) ? " " : value.trim( ) );
			}
			catch ( NoSuchElementException ex )
			{
				builder.put( key, " " );
			}
		}
		return builder.build( );
	}

	public static String getValueFromAolBodyMap( String aolBody, String key )
	{
		return getTockensMapFromAolBody( aolBody ).get( key );
	}

	public static Object [ ] getTockensFromAolBody( String aolBody )
	{
		com.google.common.collect.ImmutableList.Builder < String > builder = ImmutableList.builder( );

		StringTokenizer aolMessageTockenizer = new StringTokenizer( aolBody, "==" );
		while ( aolMessageTockenizer.hasMoreElements( ) )
		{
			String tocken = aolMessageTockenizer.nextElement( ).toString( ).trim( );
			if ( blackList.contains( tocken ) )
			{
				continue;
			}
			else
			{
				if ( ! StringUtil.isBlank( tocken ) )
				{
					builder.add( tocken );
				}
			}
		}
		ImmutableList < String > build = builder.build( );
		return build.toArray( );
	}

	public static Format createSupportedFormat( DocumentFormat sourceFormat, String description, DocumentFormat... destinationFormats )
	{
		Format format = registryFactory.createTranslatableFormatsRegistryFormat( );
		format.setFormatId( sourceFormat.format( ) );
		format.setFormatDescription( description );
		format.setAvailableTranformations( registryFactory.createTranslatableFormatsRegistryFormatAvailableTranformations( ) );

		for ( DocumentFormat destinationFormat : destinationFormats )
		{
			format.getAvailableTranformations( ).getTransformation( ).add( destinationFormat.format( ) );
		}

		return format;
	}

	public static List < String > getSupportedTranslation( )
	{
		TranslatableFormatsRegistry translatableFormatsRegistry = JaxbMapper.unmarshal( C2SDIConverter.instance.getInputStreamFromResource( "all.xml" ), TranslatableFormatsRegistry.class );
		List < String > supportedTranslation = new ArrayList <>( );

		for ( Format format : translatableFormatsRegistry.getFormat( ) )
		{
			for ( String transformation : format.getAvailableTranformations( ).getTransformation( ) )
			{
				StringBuilder buildTranslation = new StringBuilder( );
				buildTranslation.append( format.getFormatId( ) ).append( "_" ).append( transformation );
				supportedTranslation.add( buildTranslation.toString( ) );
			}
		}
		return supportedTranslation;
	}

	public static String convertAolJsonToXml( String aolMessage, Boolean trailHtml )
	{
		AOLMessage aolMessageObj = null;
		aolMessage = aolMessage.replaceAll( "\\\\&", "&" );
		try
		{
			aolMessageObj = mapper.readValue( aolMessage, AOLMessage.class );
		}
		catch ( JsonParseException e )
		{
			System.out.println( e );
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		catch ( JsonMappingException e )
		{
			System.out.println( e );
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		catch ( IOException e )
		{
			System.out.println( e );
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}

		if ( trailHtml )
		{
			String cleanBody = trailHtml( aolMessageObj.getBody( ) );
			aolMessageObj.setBody( cleanBody );
		}

		OutputStream aolXmlString = JaxbMapper.marshal( aolMessageObj );

		return aolXmlString.toString( );
	}

	public static String trailHtml( String html )
	{
		String htmlEscaped = StringEscapeUtils.unescapeHtml4( html );
		return Jsoup.parse( htmlEscaped ).text( );
	}

	public static String convertAolAckJsonToXml( String aolMessage )
	{
		AOLACKMessage aolMessageObj = null;
		try
		{
			aolMessageObj = mapper.readValue( aolMessage, AOLACKMessage.class );
		}
		catch ( JsonParseException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		catch ( JsonMappingException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		catch ( IOException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}

		OutputStream aolXmlString = JaxbMapper.marshal( aolMessageObj );

		return aolXmlString.toString( );
	}

	public static String convertIpgwJsonToXml( String ipgwString )
	{
		Root ipgwObj = null;

		try
		{
			ipgwObj = mapper.readValue( ipgwString, Root.class );
		}
		catch ( JsonParseException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		catch ( JsonMappingException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		catch ( IOException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" );
		sdf.setTimeZone( TimeZone.getTimeZone( "UTC" ) );
		String utcTime = sdf.format( new Date( ) );
		ipgwObj.setUtcTime( utcTime );
		OutputStream ipgwXmlString = JaxbMapper.marshal( ipgwObj );
		return ipgwXmlString.toString( );
	}

	public static String convertAolXmlToJson( String aolXmlString )
	{
		try
		{
			AOLMessage aolMessage;
			String jsonInString;
			aolMessage = JaxbMapper.unmarshal( IOUtils.toInputStream( aolXmlString, StandardCharsets.UTF_8.name( ) ), AOLMessage.class );
			jsonInString = mapper.writeValueAsString( aolMessage );
			return jsonInString;
		}
		catch ( IOException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
	}

	public static String convertAolAckXmlToJson( String aolAckXmlString )
	{
		try
		{
			AOLACKMessage aolMessage;
			String jsonInString;
			aolMessage = JaxbMapper.unmarshal( IOUtils.toInputStream( aolAckXmlString, StandardCharsets.UTF_8.name( ) ), AOLACKMessage.class );
			jsonInString = mapper.writeValueAsString( aolMessage );
			return jsonInString;
		}
		catch ( IOException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
	}

	public static String convertMsgSmsReceivedTxtToXml( String msgSmsReceived )
	{
		final String SOM = new String( new char [ ] { 6 } );
		final String EOB = new String( new char [ ] { 5 } );
		final String EOM = new String( new char [ ] { 15 } );

		String messageParsed = "";

		Pattern pattern = Pattern.compile( SOM + SOM + "(.*?)" + EOM + EOM );
		Matcher matcher = pattern.matcher( msgSmsReceived );
		if ( matcher.find( ) )
		{
			messageParsed = matcher.group( 1 );
		}
		else
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}

		List < String > messageParsedToken = Lists.newArrayList( messageParsed.split( EOB + EOB ) );

		MsgSmsReceived msgSmsReceivedObj = null;

		try
		{
			msgSmsReceivedObj = new MsgSmsReceived( );
			msgSmsReceivedObj.setDate( messageParsedToken.get( 0 ) );
			msgSmsReceivedObj.setIdSender( messageParsedToken.get( 1 ) );
			msgSmsReceivedObj.setDescSender( messageParsedToken.get( 2 ) );
			msgSmsReceivedObj.setIdReceiver( messageParsedToken.get( 3 ) );
			msgSmsReceivedObj.setDescReceiver( messageParsedToken.get( 4 ) );
			msgSmsReceivedObj.setIdChannel( messageParsedToken.get( 5 ) );
			msgSmsReceivedObj.setText( messageParsedToken.get( 6 ) );
			msgSmsReceivedObj.setMessagePosition( messageParsedToken.get( 7 ) );
			msgSmsReceivedObj.setAssociatedFilter( messageParsedToken.get( 8 ) );
			msgSmsReceivedObj.setIdAssociatedAction( messageParsedToken.get( 9 ) );
		}
		catch ( Exception e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}

		OutputStream msgSmsReceivedXml = JaxbMapper.marshal( msgSmsReceivedObj );

		return msgSmsReceivedXml.toString( );
	}

	public static String convertMsgSmsSendXmlToTxt( String content )
	{

		MsgSmsSend msgSmsSend = JaxbMapper.unmarshal( IOUtils.toInputStream( content ), MsgSmsSend.class );
		final String SOM = new String( new char [ ] { 6 } );
		final String EOB = new String( new char [ ] { 5 } );
		final String EOM = new String( new char [ ] { 15 } );

		StringBuilder messageParsed = new StringBuilder( ).append( SOM ).append( SOM )
			.append( msgSmsSend.getIdReceiver( ) ).append( EOB ).append( EOB )
			.append( msgSmsSend.getIdChannel( ) ).append( EOB ).append( EOB )
			.append( msgSmsSend.getText( ) ).append( EOB ).append( EOB )
			.append( msgSmsSend.getIdCommand( ) ).append( EOB ).append( EOB )
			.append( msgSmsSend.getType( ) ).append( EOB ).append( EOB )
			.append( SMS_STATION ).append( EOB ).append( EOB )
			.append( SMS_OPERATOR ).append( EOB ).append( EOB )
			.append( SMS_SEND_COMMAND )
			.append( EOM ).append( EOM );

		return messageParsed.toString( );
	}

	public static String convertMsgGpsCallReceivedToXml( String msgGpsCallReceived )
	{

		final String SOM = new String( new char [ ] { 6 } );
		final String EOB = new String( new char [ ] { 5 } );
		final String EOM = new String( new char [ ] { 15 } );

		String messageParsed = "";

		Pattern pattern = Pattern.compile( SOM + SOM + "(.*?)" + EOM + EOM );
		Matcher matcher = pattern.matcher( msgGpsCallReceived );
		if ( matcher.find( ) )
		{
			messageParsed = matcher.group( 1 );
		}

		List < String > messageParsedToken = Lists.newArrayList( messageParsed.split( EOB + EOB ) );

		MsgGpsCallReceived msgGpsCallReceivedObj = null;

		try
		{
			msgGpsCallReceivedObj = new MsgGpsCallReceived( );
			Pattern patternDate = Pattern.compile( "(\\d\\d):(\\d\\d):(\\d\\d) (\\d\\d)/(\\d\\d)/(\\d\\d\\d\\d)" );
			Matcher matcherDate = patternDate.matcher( messageParsedToken.get( 0 ) );
			StringBuilder date = new StringBuilder( );
			if ( matcherDate.find( ) )
			{
				date.append( matcherDate.group( 6 ) ).append( "-" )
					.append( matcherDate.group( 5 ) ).append( "-" )
					.append( matcherDate.group( 4 ) ).append( "T" )
					.append( matcherDate.group( 1 ) ).append( ":" )
					.append( matcherDate.group( 2 ) ).append( ":" )
					.append( matcherDate.group( 3 ) ).append( ".00" );
			}
			msgGpsCallReceivedObj.setDate( date.toString( ) );
			msgGpsCallReceivedObj.setIdSender( messageParsedToken.get( 1 ) );
			msgGpsCallReceivedObj.setDescSender( messageParsedToken.get( 2 ) );
			msgGpsCallReceivedObj.setIdReceiver( messageParsedToken.get( 3 ) );
			msgGpsCallReceivedObj.setDescReceiver( messageParsedToken.get( 4 ) );
			msgGpsCallReceivedObj.setCounter( messageParsedToken.get( 5 ) );
			msgGpsCallReceivedObj.setPackControl( messageParsedToken.get( 6 ) );
			msgGpsCallReceivedObj.setEtsType( messageParsedToken.get( 7 ) );
			msgGpsCallReceivedObj.setLatitude( messageParsedToken.get( 8 ) );
			msgGpsCallReceivedObj.setLongitude( messageParsedToken.get( 9 ) );
			msgGpsCallReceivedObj.setSpeed( messageParsedToken.get( 10 ) );
			msgGpsCallReceivedObj.setAngle( messageParsedToken.get( 11 ) );
			msgGpsCallReceivedObj.setNumOfSatellites( messageParsedToken.get( 12 ) );
			msgGpsCallReceivedObj.setSynchronism( messageParsedToken.get( 13 ) );
			msgGpsCallReceivedObj.setOutputs( messageParsedToken.get( 14 ) );
			msgGpsCallReceivedObj.setInputs( messageParsedToken.get( 15 ) );
			msgGpsCallReceivedObj.setStateOfMobileUnit( messageParsedToken.get( 16 ) );
			msgGpsCallReceivedObj.setIdChannel( messageParsedToken.get( 17 ) );
			msgGpsCallReceivedObj.setIdCommand( messageParsedToken.get( 18 ) );
		}
		catch ( Exception e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred in convertMsgGpsCallReceivedToXml, please try again later." );
		}

		msgGpsCallReceivedObj = convertCoordinate( msgGpsCallReceivedObj );

		OutputStream msgGpsCallReceivedXml = JaxbMapper.marshal( msgGpsCallReceivedObj );

		return msgGpsCallReceivedXml.toString( );
	}

	private static MsgGpsCallReceived convertCoordinate( MsgGpsCallReceived msgGpsCallReceivedObj )
	{
		Double latitude = new Double( msgGpsCallReceivedObj.getLatitude( ) );
		Double longitude = new Double( msgGpsCallReceivedObj.getLongitude( ) );

		Double longitudeConverted = longitude / 100000;
		Double latitudeConverted = latitude / 100000;

		msgGpsCallReceivedObj.setLatitude( latitudeConverted.toString( ) );
		msgGpsCallReceivedObj.setLongitude( longitudeConverted.toString( ) );

		return msgGpsCallReceivedObj;
	}

	public static String generatePoint( String areaValue )
	{
		StringTokenizer tockenizer = new StringTokenizer( areaValue, " " );
		StringBuilder sb = new StringBuilder( "POINT(" );

		if ( tockenizer.hasMoreTokens( ) )
		{
			String tocken = tockenizer.nextToken( );
			sb.append( tocken );
		}
		sb.append( ")" );
		return sb.toString( );
	}

	public static String generatePolygonFromAOLGeometry( String aolGermetry )
	{
		final String regex = "(?<=POINT\\()(.*)(?=\\))";

		final Pattern pattern = Pattern.compile( regex );
		final Matcher matcher = pattern.matcher( aolGermetry );
		String points = null;
		while ( matcher.find( ) )
		{
			points = matcher.group( 0 );
		}

		// Append 4 times the same coordinates to build polygon compliant with cap.
		StringBuilder sb = new StringBuilder( );
		sb.append( points ).append( " " ).append( points ).append( " " ).append( points ).append( " " ).append( points );

		return sb.toString( );
	}

	public static String generateAOLId( )
	{
		return UUID.randomUUID( ).toString( );
	}

	public static String generateUUID( )
	{
		return UUID.randomUUID( ).toString( );
	}

	/* VERY OLD STUFF */

	@Deprecated
	public static Long extractNumberFromBody( String body )
	{
		final String regex = "#(.*?)#";
		Long extractedNumber = 1L;

		final Pattern pattern = Pattern.compile( regex );
		final Matcher matcher = pattern.matcher( body );

		while ( matcher.find( ) )
		{
			for ( int i = 1; i <= matcher.groupCount( ); i ++ )
			{
				extractedNumber = Long.valueOf( matcher.group( i ) );
			}
		}
		return extractedNumber;
	}

	@Deprecated
	public static Long extractAolId( String string )
	{
		final String regex = "\"(.*?)\"";
		Long extractedAOLId = null;

		final Pattern pattern = Pattern.compile( regex );
		final Matcher matcher = pattern.matcher( string );

		while ( matcher.find( ) )
		{
			for ( int i = 1; i <= matcher.groupCount( ); i ++ )
			{
				extractedAOLId = Long.valueOf( matcher.group( i ) );
			}
		}
		return extractedAOLId;
	}
	/* /VERY OLD STUFF */
}
