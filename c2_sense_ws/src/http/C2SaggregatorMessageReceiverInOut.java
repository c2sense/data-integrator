/**
 * C2SaggregatorMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package http;




/**
 * C2SaggregatorMessageReceiverInOut message receiver
 */

public class C2SaggregatorMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver
{

	public void invokeBusinessLogic( org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext )
		throws org.apache.axis2.AxisFault
	{

		try
		{

			// get the implementation class for the Web Service
			Object obj = getTheImplementationObject( msgContext );

			C2SaggregatorSkeleton skel = ( C2SaggregatorSkeleton ) obj;
			// Out Envelop
			org.apache.axiom.soap.SOAPEnvelope envelope = null;
			// Find the axisOperation that has been set by the Dispatch phase.
			org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext( ).getAxisOperation( );
			if ( op == null )
			{
				throw new org.apache.axis2.AxisFault( "Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider" );
			}

			java.lang.String methodName;
			if ( ( op.getName( ) != null ) && ( ( methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier( op.getName( ).getLocalPart( ) ) ) != null ) )
			{

				if ( "c2SaggregatorSupportedFormat".equals( methodName ) )
				{

					http.C2SaggregatorSupportedFormatResponse c2SaggregatorSupportedFormatResponse1 = null;
					http.C2SaggregatorSupportedFormat wrappedParam =
						( http.C2SaggregatorSupportedFormat ) fromOM(
							msgContext.getEnvelope( ).getBody( ).getFirstElement( ),
							http.C2SaggregatorSupportedFormat.class,
							getEnvelopeNamespaces( msgContext.getEnvelope( ) ) );

					c2SaggregatorSupportedFormatResponse1 =

						skel.c2SaggregatorSupportedFormat( wrappedParam );

					envelope = toEnvelope( getSOAPFactory( msgContext ), c2SaggregatorSupportedFormatResponse1, false, new javax.xml.namespace.QName( "urn:http://localhost/c2sense/wsdl/c2saggregator",
						"c2SaggregatorSupportedFormat" ) );
				}
				else

				if ( "c2SaggregatorReceiveEvent".equals( methodName ) )
				{

					http.C2SaggregatorReceiveEventResponse c2SaggregatorReceiveEventResponse3 = null;
					http.C2SaggregatorReceiveEvent wrappedParam =
						( http.C2SaggregatorReceiveEvent ) fromOM(
							msgContext.getEnvelope( ).getBody( ).getFirstElement( ),
							http.C2SaggregatorReceiveEvent.class,
							getEnvelopeNamespaces( msgContext.getEnvelope( ) ) );

					c2SaggregatorReceiveEventResponse3 =

						skel.c2SaggregatorReceiveEvent( wrappedParam );

					envelope = toEnvelope( getSOAPFactory( msgContext ), c2SaggregatorReceiveEventResponse3, false, new javax.xml.namespace.QName( "urn:http://localhost/c2sense/wsdl/c2saggregator",
						"c2SaggregatorReceiveEvent" ) );

				}
				else
				{
					throw new java.lang.RuntimeException( "method not found" );
				}

				newMsgContext.setEnvelope( envelope );
			}
		}
		catch ( java.lang.Exception e )
		{
			throw org.apache.axis2.AxisFault.makeFault( e );
		}
	}

	//
	private org.apache.axiom.om.OMElement toOM( http.C2SaggregatorSupportedFormat param, boolean optimizeContent )
		throws org.apache.axis2.AxisFault
	{

		try
		{
			return param.getOMElement( http.C2SaggregatorSupportedFormat.MY_QNAME,
				org.apache.axiom.om.OMAbstractFactory.getOMFactory( ) );
		}
		catch ( org.apache.axis2.databinding.ADBException e )
		{
			throw org.apache.axis2.AxisFault.makeFault( e );
		}

	}

	private org.apache.axiom.om.OMElement toOM( http.C2SaggregatorSupportedFormatResponse param, boolean optimizeContent )
		throws org.apache.axis2.AxisFault
	{

		try
		{
			return param.getOMElement( http.C2SaggregatorSupportedFormatResponse.MY_QNAME,
				org.apache.axiom.om.OMAbstractFactory.getOMFactory( ) );
		}
		catch ( org.apache.axis2.databinding.ADBException e )
		{
			throw org.apache.axis2.AxisFault.makeFault( e );
		}

	}

	private org.apache.axiom.om.OMElement toOM( http.C2SaggregatorReceiveEvent param, boolean optimizeContent )
		throws org.apache.axis2.AxisFault
	{

		try
		{
			return param.getOMElement( http.C2SaggregatorReceiveEvent.MY_QNAME,
				org.apache.axiom.om.OMAbstractFactory.getOMFactory( ) );
		}
		catch ( org.apache.axis2.databinding.ADBException e )
		{
			throw org.apache.axis2.AxisFault.makeFault( e );
		}

	}

	private org.apache.axiom.om.OMElement toOM( http.C2SaggregatorReceiveEventResponse param, boolean optimizeContent )
		throws org.apache.axis2.AxisFault
	{

		try
		{
			return param.getOMElement( http.C2SaggregatorReceiveEventResponse.MY_QNAME,
				org.apache.axiom.om.OMAbstractFactory.getOMFactory( ) );
		}
		catch ( org.apache.axis2.databinding.ADBException e )
		{
			throw org.apache.axis2.AxisFault.makeFault( e );
		}

	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope( org.apache.axiom.soap.SOAPFactory factory, http.C2SaggregatorSupportedFormatResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName )
		throws org.apache.axis2.AxisFault
	{
		try
		{
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope( );

			emptyEnvelope.getBody( ).addChild( param.getOMElement( http.C2SaggregatorSupportedFormatResponse.MY_QNAME, factory ) );

			return emptyEnvelope;
		}
		catch ( org.apache.axis2.databinding.ADBException e )
		{
			throw org.apache.axis2.AxisFault.makeFault( e );
		}
	}

	private http.C2SaggregatorSupportedFormatResponse wrapc2SaggregatorSupportedFormat( )
	{
		http.C2SaggregatorSupportedFormatResponse wrappedElement = new http.C2SaggregatorSupportedFormatResponse( );
		return wrappedElement;
	}

	private org.apache.axiom.soap.SOAPEnvelope toEnvelope( org.apache.axiom.soap.SOAPFactory factory, http.C2SaggregatorReceiveEventResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName )
		throws org.apache.axis2.AxisFault
	{
		try
		{
			org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope( );

			emptyEnvelope.getBody( ).addChild( param.getOMElement( http.C2SaggregatorReceiveEventResponse.MY_QNAME, factory ) );

			return emptyEnvelope;
		}
		catch ( org.apache.axis2.databinding.ADBException e )
		{
			throw org.apache.axis2.AxisFault.makeFault( e );
		}
	}

	private http.C2SaggregatorReceiveEventResponse wrapc2SaggregatorReceiveEvent( )
	{
		http.C2SaggregatorReceiveEventResponse wrappedElement = new http.C2SaggregatorReceiveEventResponse( );
		return wrappedElement;
	}

	/**
	 * get the default envelope
	 */
	private org.apache.axiom.soap.SOAPEnvelope toEnvelope( org.apache.axiom.soap.SOAPFactory factory )
	{
		return factory.getDefaultEnvelope( );
	}

	private java.lang.Object fromOM(
		org.apache.axiom.om.OMElement param,
		java.lang.Class type,
		java.util.Map extraNamespaces ) throws org.apache.axis2.AxisFault
	{

		try
		{

			if ( http.C2SaggregatorReceiveEvent.class.equals( type ) )
			{

				return http.C2SaggregatorReceiveEvent.Factory.parse( param.getXMLStreamReaderWithoutCaching( ) );

			}

			if ( http.C2SaggregatorReceiveEventResponse.class.equals( type ) )
			{

				return http.C2SaggregatorReceiveEventResponse.Factory.parse( param.getXMLStreamReaderWithoutCaching( ) );

			}

			if ( http.C2SaggregatorSupportedFormat.class.equals( type ) )
			{

				return http.C2SaggregatorSupportedFormat.Factory.parse( param.getXMLStreamReaderWithoutCaching( ) );

			}

			if ( http.C2SaggregatorSupportedFormatResponse.class.equals( type ) )
			{

				return http.C2SaggregatorSupportedFormatResponse.Factory.parse( param.getXMLStreamReaderWithoutCaching( ) );

			}

		}
		catch ( java.lang.Exception e )
		{
			throw org.apache.axis2.AxisFault.makeFault( e );
		}
		return null;
	}

	/**
	 * A utility method that copies the namepaces from the SOAPEnvelope
	 */
	private java.util.Map getEnvelopeNamespaces( org.apache.axiom.soap.SOAPEnvelope env )
	{
		java.util.Map returnMap = new java.util.HashMap( );
		java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces( );
		while ( namespaceIterator.hasNext( ) )
		{
			org.apache.axiom.om.OMNamespace ns = ( org.apache.axiom.om.OMNamespace ) namespaceIterator.next( );
			returnMap.put( ns.getPrefix( ), ns.getNamespaceURI( ) );
		}
		return returnMap;
	}

	private org.apache.axis2.AxisFault createAxisFault( java.lang.Exception e )
	{
		org.apache.axis2.AxisFault f;
		Throwable cause = e.getCause( );
		if ( cause != null )
		{
			f = new org.apache.axis2.AxisFault( e.getMessage( ), cause );
		}
		else
		{
			f = new org.apache.axis2.AxisFault( e.getMessage( ) );
		}

		return f;
	}

}// end of class
