/**
 * C2SaggregatorSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package http;



import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import util.C2SDIUtils;
import util.DocumentFormat;
import converter.C2SDIConverter;





/**
 * C2SaggregatorSkeleton java skeleton for the axisService
 */
public class C2SaggregatorSkeleton
{

	private static Logger	logger	= Logger.getLogger( C2SaggregatorSkeleton.class );

	/**
	 * Auto generated method signature Send supported format list.
	 * 
	 * @param c2SaggregatorSupportedFormat
	 * @return c2SaggregatorSupportedFormatResponse
	 */

	public http.C2SaggregatorSupportedFormatResponse c2SaggregatorSupportedFormat(
		http.C2SaggregatorSupportedFormat c2SaggregatorSupportedFormat )
	{

		String supportedFormat = c2SaggregatorSupportedFormat.localSupportedFormat;
		C2SaggregatorSupportedFormatResponse response = new C2SaggregatorSupportedFormatResponse( );

		try
		{
			boolean throwException = supportedFormat.equals( "" )
				|| supportedFormat.equals( " " );
			if ( throwException )
			{
				throw new java.lang.UnsupportedOperationException(
					"Please specify format." );
			}
		}
		catch ( Exception e )
		{
			throw new java.lang.UnsupportedOperationException(
				"Please specify format." );
		}

		DataSource ds;
		try
		{
			StringBuilder sb = new StringBuilder( supportedFormat );
			InputStream input = this.getClass( ).getClassLoader( ).getResourceAsStream( sb.append( ".xml" ).toString( ) );
			if ( input == null )
			{
				throw new java.lang.UnsupportedOperationException( "Format not supported." );
			}
			ds = new ByteArrayDataSource( input, "application/xml; charset=UTF-8" );
			DataHandler dataHandler = new DataHandler( ds );
			response.setDestinationDocument( dataHandler );
		}
		catch ( IOException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some error occurred." );
		}

		return response;
	}

	/**
	 * Auto generated method signature Receive, Validate and Convert data.
	 * 
	 * @param c2SaggregatorReceiveEvent
	 * @return c2SaggregatorReceiveEventResponse
	 */

	public http.C2SaggregatorReceiveEventResponse c2SaggregatorReceiveEvent(
		http.C2SaggregatorReceiveEvent c2SaggregatorReceiveEvent )
	{

		String localDestinationFormat = c2SaggregatorReceiveEvent.localDestinationFormat;
		String localSourceFormat = c2SaggregatorReceiveEvent.localSourceFormat;
		C2SaggregatorReceiveEventResponse response = new C2SaggregatorReceiveEventResponse( );
		ByteArrayOutputStream inputFileLocalCache = new ByteArrayOutputStream( );

		// Controllo che i formati richiesti siano supportati
		try
		{
			boolean throwException = localDestinationFormat.equals( "" )
				|| localSourceFormat.equals( "" )
				|| localDestinationFormat.equals( " " )
				|| localSourceFormat.equals( " " );
			if ( throwException )
			{
				throw new java.lang.UnsupportedOperationException( "Please specify both source and destination format." );
			}
		}
		catch ( Exception e )
		{
			throw new java.lang.UnsupportedOperationException( "Please specify both source and destination format." );
		}

		if ( ! C2SDIConverter.instance.isSourceFormatSupported( localSourceFormat ) )
		{
			throw new java.lang.UnsupportedOperationException( "Unsupported source format." );
		}

		if ( ! C2SDIConverter.instance.isDestinationFormatSupported( localDestinationFormat ) )
		{
			throw new java.lang.UnsupportedOperationException( "Unsupported destination format." );
		}

		logger.debug( "TRANSOFRMATION REQUESTED FROM " + localSourceFormat + " TO " + localDestinationFormat );

		// Cache dell'inputStream.
		try
		{
			IOUtils.copy( c2SaggregatorReceiveEvent.localSourceDocument.getInputStream( ), inputFileLocalCache );
		}
		catch ( IOException ex )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}

		// Converto da formato json a xml IPGW e AOL
		if ( localSourceFormat.equals( DocumentFormat.IPGW_JSON.format( ) ) )
		{
			String convertIpgwXml;
			try
			{
				convertIpgwXml = C2SDIUtils.convertIpgwJsonToXml( inputFileLocalCache.toString( ) );

				if ( convertIpgwXml == null )
				{
					logger.debug( "INVALID IPGW MESSAGE" );
					throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
				}
				inputFileLocalCache = new ByteArrayOutputStream( );
				IOUtils.copy( IOUtils.toInputStream( convertIpgwXml ), inputFileLocalCache );
			}
			catch ( IOException e )
			{
				throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
			}
		}

		if ( localSourceFormat.equals( DocumentFormat.AOL_JSON.format( ) ) )
		{
			String convertAolXml;
			try
			{
				convertAolXml = C2SDIUtils.convertAolJsonToXml( IOUtils.toString( c2SaggregatorReceiveEvent.localSourceDocument.getInputStream( ), StandardCharsets.UTF_8.name( ) ), true );
				if ( convertAolXml == null )
				{
					logger.debug( "INVALID AOL MESSAGE" );
					throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
				}
				inputFileLocalCache = new ByteArrayOutputStream( );
				IOUtils.copy( IOUtils.toInputStream( convertAolXml, StandardCharsets.UTF_8.name( ) ), inputFileLocalCache );
			}
			catch ( IOException e )
			{
				throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
			}
		}

		if ( localSourceFormat.equals( DocumentFormat.AOL_ACK_JSON.format( ) ) )
		{
			String convertAolXml;
			try
			{
				convertAolXml = C2SDIUtils.convertAolJsonToXml( IOUtils.toString( c2SaggregatorReceiveEvent.localSourceDocument.getInputStream( ), StandardCharsets.UTF_8.name( ) ), true );
				if ( convertAolXml == null )
				{
					logger.debug( "INVALID AOLACK MESSAGE" );
					throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
				}
				inputFileLocalCache = new ByteArrayOutputStream( );
				IOUtils.copy( IOUtils.toInputStream( convertAolXml, StandardCharsets.UTF_8.name( ) ), inputFileLocalCache );
			}
			catch ( IOException e )
			{
				throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
			}
		}

		if ( localSourceFormat.equals( DocumentFormat.MSG_SMS_RECEIVED_TXT.format( ) ) )
		{
			String convertMsgSmsReceivedXml;
			try
			{
				convertMsgSmsReceivedXml = C2SDIUtils.convertMsgSmsReceivedTxtToXml( IOUtils.toString( c2SaggregatorReceiveEvent.localSourceDocument.getInputStream( ), StandardCharsets.UTF_8.name( ) ) );
				if ( convertMsgSmsReceivedXml == null )
				{
					logger.debug( "INVALID TRBONET MESSAGE" );
					throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
				}
				inputFileLocalCache = new ByteArrayOutputStream( );
				IOUtils.copy( IOUtils.toInputStream( convertMsgSmsReceivedXml, StandardCharsets.UTF_8.name( ) ), inputFileLocalCache );
			}
			catch ( IOException e )
			{
				throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
			}
		}

		if ( localSourceFormat.equals( DocumentFormat.EDXL_MP_XML.format( ) ) )
		{
			try
			{
				inputFileLocalCache = new ByteArrayOutputStream( );
				IOUtils.copy( c2SaggregatorReceiveEvent.localSourceDocument.getInputStream( ), inputFileLocalCache );
			}
			catch ( IOException e )
			{
				logger.debug( "INVALID MP MESSAGE" );
				throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
			}
		}

		if ( localSourceFormat.equals( DocumentFormat.MSG_GPS_CALL_RECEIVED_TXT.format( ) ) )
		{
			String convertMsgGpsCallReceivedXml;
			try
			{
				convertMsgGpsCallReceivedXml = C2SDIUtils.convertMsgGpsCallReceivedToXml( IOUtils.toString( c2SaggregatorReceiveEvent.localSourceDocument.getInputStream( ), StandardCharsets.UTF_8.name( ) ) );
				if ( convertMsgGpsCallReceivedXml == null )
				{
					logger.debug( "INVALID TRBONET MESSAGE" );
					throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
				}
				inputFileLocalCache = new ByteArrayOutputStream( );
				IOUtils.copy( IOUtils.toInputStream( convertMsgGpsCallReceivedXml, StandardCharsets.UTF_8.name( ) ), inputFileLocalCache );
			}
			catch ( IOException e )
			{
				throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
			}
		}

		try
		{
			// Valido il documento in ingresso che deve essere convertito
			StreamSource streamSource = new StreamSource( new ByteArrayInputStream( inputFileLocalCache.toByteArray( ) ) );
			Boolean isInputValid = C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + localSourceFormat + ".xsd", streamSource );
			if ( isInputValid == null )
			{
				logger.debug( "INVALID MESSAGE" );
				throw new java.lang.UnsupportedOperationException( );
			}
		}
		catch ( UnsupportedOperationException e )
		{
			throw new java.lang.UnsupportedOperationException( "Sorry the file you request to convert is not a valid " + localSourceFormat + ". " + e.getMessage( ) );
		}

		if ( localSourceFormat.equals( localDestinationFormat ) )
		{
			DataSource ds;
			try
			{
				ds = new ByteArrayDataSource( inputFileLocalCache.toString( ), "text/plain; charset=UTF-8" );
			}
			catch ( IOException e )
			{
				throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
			}

			DataHandler dataHandler = new DataHandler( ds );
			response.setDestinationDocument( dataHandler );

			return response;
		}

		// Controllo che la trasformazione sia supportata.
		String transofrmation = localSourceFormat + "_" + localDestinationFormat;
		Transformer transformer = C2SDIConverter.instance.getTransformer( transofrmation );
		if ( transformer == null )
		{
			logger.debug( "UNSUPPRTED: " + transofrmation );
			throw new java.lang.UnsupportedOperationException( "Unsupported transformation." );
		}

		// Conversione
		StringWriter conversionWriter = C2SDIConverter.instance.convert( new ByteArrayInputStream( inputFileLocalCache.toByteArray( ) ), transformer );

		// Valido output
		try
		{
			StreamSource validateThisOutput = new StreamSource( IOUtils.toInputStream( conversionWriter.toString( ) ) );
			C2SDIConverter.instance.validateXMLSchemaURL( "schemas/" + localDestinationFormat + ".xsd", validateThisOutput );
		}
		catch ( UnsupportedOperationException e )
		{
			throw new java.lang.UnsupportedOperationException( "Unable to produce a valid " + localDestinationFormat + ". Please check that your input has all required destination fields. " + e.getMessage( ) );
		}

		DataSource ds;
		try
		{
			String responseMessage = conversionWriter.toString( );
			if ( localSourceFormat.equals( DocumentFormat.EDXL_MP_XML.format( ) ) && localDestinationFormat.equals( DocumentFormat.MSG_SMS_SEND_TXT.format( ) ) )
			{
				responseMessage = C2SDIUtils.convertMsgSmsSendXmlToTxt( conversionWriter.toString( ) );
			}
			if ( localDestinationFormat.equals( DocumentFormat.AOL_JSON.format( ) ) )
			{
				responseMessage = C2SDIUtils.convertAolXmlToJson( conversionWriter.toString( ) );
			}
			ds = new ByteArrayDataSource( responseMessage, "text/plain; charset=UTF-8" );
		}
		catch ( IOException e )
		{
			logger.debug( "ERROR WHILE CONVERTING" );
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}

		DataHandler dataHandler = new DataHandler( ds );
		response.setDestinationDocument( dataHandler );

		return response;
	}
}
