
package main;



import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;

import util.C2SDIUtils;
import converter.C2SDIConverter;





public class Main
{
	public static void main( String [ ] args )
	{
		// System.out.println( C2SDIUtils.cutMessageLength( "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum" ));
		
		// System.out.println( C2SDIUtils.generatePoint( "47.976223,16.507974 0" ) );
		
		// System.out.println(C2SDIUtils.ZuluDate( ));
		
		// System.out.println(C2SDIUtils.extractMessageFromEMTCap( "message_info from Road Problem_6d960bed-22b2-4aa5-9da2-5e4cd585debc: {\"active\": true, \"text\": \"highway blocked, pay attention!!!\", \"message\": \"road_status_blocked\", \"messageId\": \"b09f3256-2549-4280-bed4-18ac09113ca8\"}" ));
		
		System.out.println(C2SDIUtils.generatePolygonFromAOLGeometry( "POINT(41.4487138524583,15.5662273017578)" ));
	}

	public static void capToSITREPConversion( )
	{
		InputStream capAlert = C2SDIConverter.instance.getInputStreamFromResource( "xml/cap_alert.xml" );
		C2SDIConverter.instance.capToSiterep( capAlert );
	}

	public static void ipgwJsonToOGCOMConversion( )
	{
		String convertIpgwJsonToXml = null;
		try
		{
			convertIpgwJsonToXml = C2SDIUtils.convertIpgwJsonToXml( IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/ipgw.json" ) ) );
		}
		catch ( IOException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		StringWriter ipgwJsonToOgcConversion = C2SDIConverter.instance.ipgwToOgcConversion( IOUtils.toInputStream( convertIpgwJsonToXml ) );
		if ( ipgwJsonToOgcConversion != null )
		{
			System.out.println( "=====================" );
			System.out.println( "IPGW JSON TO OGC OM WORKS!" );
			System.out.println( "=====================" );
		}
	}

	public static void aolToCapConversion( )
	{
		InputStream aolMessage = C2SDIConverter.instance.getInputStreamFromResource( "xml/aol_message.xml" );
		StringWriter aolToCapConversion = C2SDIConverter.instance.aolToCapConversion( aolMessage );

		if ( aolToCapConversion != null )
		{
			System.out.println( "=====================" );
			System.out.println( "AOL TO CAP WORKS!" );
			System.out.println( "=====================" );
		}
	}

	public static void aolJsonToCapConversion( )
	{
		String convertAolJsonToXml = null;
		try
		{
			convertAolJsonToXml = C2SDIUtils.convertAolJsonToXml( IOUtils.toString( C2SDIConverter.instance.getInputStreamFromResource( "xml/aol.json" ) ), true );
		}
		catch ( IOException e )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}
		StringWriter aolJsonToCapConversion = C2SDIConverter.instance.aolToCapConversion( IOUtils.toInputStream( convertAolJsonToXml ) );
		if ( aolJsonToCapConversion != null )
		{
			System.out.println( "=====================" );
			System.out.println( "AOL JSON TO CAP WORKS!" );
			System.out.println( "=====================" );
		}
	}

	public static void capToHTH( )
	{
		InputStream capAlert = C2SDIConverter.instance.getInputStreamFromResource( "xml/cap_alert.xml" );
		StringWriter capToHTH = C2SDIConverter.instance.capToHTHConversion( capAlert );

		if ( capToHTH != null )
		{
			System.out.println( "=====================" );
			System.out.println( "CAP TO HTH WORKS!" );
			System.out.println( "=====================" );
		}
	}
}
