
package converter;



import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.XMLConstants;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.xml.sax.SAXException;

import util.DocumentFormat;





public enum C2SDIConverter
{
	instance;

	private TransformerFactory	factory;
	private SchemaFactory		schemaFactory;

	private C2SDIConverter( )
	{
		factory = TransformerFactory.newInstance( );
		schemaFactory = SchemaFactory.newInstance( XMLConstants.W3C_XML_SCHEMA_NS_URI );
	}

	public String helloWorld( )
	{
		return "Hello World";
	}

	public boolean isSourceFormatSupported( String sourceFormat )
	{
		return DocumentFormat.getSupportedSourceFormat( ).contains( sourceFormat.toLowerCase( ) );
	}

	public boolean isDestinationFormatSupported( String destinationFormat )
	{
		return DocumentFormat.getSupportedDestinationFormat( ).contains( destinationFormat.toLowerCase( ) );
	}

	public Transformer getTransformer( String xslName )
	{
		try
		{
			StringBuilder xsltResourceFolder = new StringBuilder( "xslt/" );
			InputStream input = getInputStreamFromResource( xsltResourceFolder.append( xslName.toLowerCase( ) ).append( ".xsl" ).toString( ) );

			if ( input == null )
			{
				return null;
			}

			Templates template = factory.newTemplates( new StreamSource( input ) );
			Transformer xformer = template.newTransformer( );
			return xformer;
		}
		catch ( TransformerConfigurationException ex )
		{
			return null;
		}
	}

	public StringWriter aolToCapConversion( InputStream aolMessage )
	{
		Transformer transformer = getTransformer( "aol.json_cap.xml.xsl" );

		ByteArrayOutputStream baos = new ByteArrayOutputStream( );
		try
		{
			IOUtils.copy( aolMessage, baos );
		}
		catch ( IOException ex )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later." );
		}

		validateXMLSchemaURL( "schemas/aol_json/aol.json.xsd", new StreamSource( new ByteArrayInputStream( baos.toByteArray( ) ) ) );

		StringWriter writer = new StringWriter( );
		Result result = new StreamResult( writer );

		try
		{
			transformer.transform( new StreamSource( new ByteArrayInputStream( baos.toByteArray( ) ) ), result );
		}
		catch ( TransformerException ex )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later. " + ex.getMessage( ) );
		}

		return writer;
	}

	public StringWriter capToHTHConversion( InputStream capAlert )
	{
		Transformer transformer = getTransformer( "cap.xml_h2h.xml.xsl" );

		Source xmlStreamSource = new StreamSource( capAlert );
		StringWriter writer = new StringWriter( );
		Result result = new StreamResult( writer );

		try
		{
			transformer.transform( xmlStreamSource, result );
		}
		catch ( TransformerException ex )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later. " + ex.getMessage( ) );
		}

		if ( validateXMLSchemaURL( "schemas/h2h/h2h.xsd", new StreamSource( new StringReader( writer.toString( ) ) ) ) )
		{
			return writer;
		}

		return null;
	}

	public StringWriter capToSiterep( InputStream capAlert )
	{
		Transformer transformer = getTransformer( "cap.xml_sitrep.xml.xsl" );

		Source xmlStreamSource = new StreamSource( capAlert );
		StringWriter writer = new StringWriter( );
		Result result = new StreamResult( writer );

		try
		{
			transformer.transform( xmlStreamSource, result );
		}
		catch ( TransformerException ex )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later. " + ex.getMessage( ) );
		}

		if ( validateXMLSchemaURL( "schemas/sitrep/EDXLSitRep-v1.0.xsd", new StreamSource( new StringReader( writer.toString( ) ) ) ) )
		{
			return writer;
		}

		return null;

	}

	public InputStream getInputStreamFromResource( String findThisInResources )
	{
		InputStream input = this.getClass( ).getClassLoader( ).getResourceAsStream( findThisInResources );
		return input;
	}

	public URL getUrlStreamFromResource( String xml )
	{
		URL input = this.getClass( ).getClassLoader( ).getResource( xml );
		return input;
	}

	public String getPathFromResource( String findThisInResources )
	{
		String path = this.getClass( ).getClassLoader( ).getResource( findThisInResources ).getPath( );
		return path;
	}

	public Boolean validateXMLSchemaURL( String xsd, StreamSource xml )
	{
		try
		{
			URL input = getUrlStreamFromResource( xsd );
			if ( input == null )
			{
				return null;
			}
			Schema schema = schemaFactory.newSchema( input );
			Validator validator = schema.newValidator( );
			validator.validate( xml );
		}
		catch ( IOException | SAXException e )
		{
			throw new java.lang.UnsupportedOperationException( e.getMessage( ) );
		}
		return true;
	}

	public static String UTCDate( )
	{
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss Z" );
		sdf.setTimeZone( TimeZone.getTimeZone( "UTC" ) );
		return sdf.format( new Date( ) );
	}

	public StringWriter convert( InputStream convertThis, Transformer transformer )
	{
		Source xmlToConvert = new StreamSource( convertThis );
		StringWriter writer = new StringWriter( );
		Result result = new StreamResult( writer );
		try
		{
			transformer.transform( xmlToConvert, result );
		}
		catch ( TransformerException ex )
		{
			System.out.println(ex.toString( ));
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later. " + ex.getMessage( ) );
		}

		return writer;
	}

	public StringWriter ipgwToOgcConversion( InputStream ipgwInputStream )
	{
		Transformer transformer = getTransformer( "ipgw.json_ogc.om.xml.xsl" );

		Source xmlStreamSource = new StreamSource( ipgwInputStream );
		StringWriter writer = new StringWriter( );
		Result result = new StreamResult( writer );

		try
		{
			transformer.transform( xmlStreamSource, result );
		}
		catch ( TransformerException ex )
		{
			throw new java.lang.UnsupportedOperationException( "Some errors occurred, please try again later. " + ex.getMessage( ) );
		}

		return writer;
	}
}
