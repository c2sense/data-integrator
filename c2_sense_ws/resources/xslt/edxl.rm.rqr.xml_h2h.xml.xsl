<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HtH_Message>
			<HtH_Content>
Resource Type: <xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ResourceInformation"]/*[local-name() = "ResourceInfoElementID"]'/>
Description: <xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ResourceInformation"]/*[local-name() = "Resource"]/*[local-name() = "Description"]'/>
	</HtH_Content>
	<HtH_Property>
		<HtH_PropertyKey>Subject</HtH_PropertyKey>
		<HtH_PropertyValue><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "IncidentInformation"]/*[local-name() = "IncidentDescription"]'/></HtH_PropertyValue>
	</HtH_Property>
	<HtH_Property>
		<HtH_PropertyKey>Format</HtH_PropertyKey>
		<HtH_PropertyValue>text/plain</HtH_PropertyValue>
	</HtH_Property>
</HtH_Message>
	</xsl:template>
</xsl:stylesheet>