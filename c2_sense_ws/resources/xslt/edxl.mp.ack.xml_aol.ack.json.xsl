<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg" 
	xmlns:b="urn:oasis:names:tc:emergency:EDXL:RM:1.0"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
	<xsl:template match="/">
	<AOLACKMessage>
	    <MessageID><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLACKMessage"]/*[local-name() = "MessageID"]' /></MessageID>
	    <From>
	        <AOLNetworkID><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLACKMessage"]/*[local-name() = "From"]/*[local-name() = "AOLNetworkID"]'/></AOLNetworkID>
				<Name><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLACKMessage"]/*[local-name() = "From"]/*[local-name() = "Name"]'/></Name>
	    </From>
	    <Status><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLACKMessage"]/*[local-name() = "Status"]'/></Status>
	</AOLACKMessage>
	</xsl:template>
</xsl:stylesheet>