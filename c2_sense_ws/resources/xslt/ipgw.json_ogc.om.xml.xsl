<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<om:OM_Observation xmlns:om='http://www.opengis.net/om/2.0' xmlns:gml='http://www.opengis.net/gml/3.2' xmlns:swe='http://www.opengis.net/swe/1.0/gml32' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' gml:id='O35235' xsi:schemaLocation='http://www.opengis.net/om/2.0 http://schemas.opengis.net/om/2.0/observation.xsd'>
			<gml:name>C2SENSE observation</gml:name>
			<om:type xlink:href='http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_SWEObservation' />
			<om:phenomenonTime>
				<gml:TimePeriod gml:id='TP2523'>
					<gml:beginPosition>
						<xsl:value-of select="root/measures[1]/time" />
					</gml:beginPosition>
					<gml:endPosition>
						<xsl:value-of select="root/measures[last()]/time" />
					</gml:endPosition>
				</gml:TimePeriod>
			</om:phenomenonTime>
			<om:resultTime>
				<gml:TimeInstant gml:id='eTP2523'>
					<gml:timePosition>
						<xsl:value-of select="root/utcTime" />
					</gml:timePosition>
				</gml:TimeInstant>
			</om:resultTime>
			<om:procedure xlink:href='urn:c2-sense:sensor:observations' />
			<om:observedProperty xlink:href='urn:c2-sense:sensor:sensornetwork' />
			<om:featureOfInterest xlink:title="IPGW" xlink:href="http://IPGW.pl/" />
			<om:result>
				<swe:DataArray>
					<swe:elementCount>
						<swe:Count>
							<swe:value>
								<xsl:value-of select="count(root/measures)" />
							</swe:value>
						</swe:Count>
					</swe:elementCount>
					<swe:elementType name='IPGW'>
						<swe:DataRecord xmlns:gml="http://www.opengis.net/gml" xmlns:swe="http://www.opengis.net/swe/1.0.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/swe/1.0.1 http://schemas.opengis.net/sweCommon/1.0.1/swe.xsd">
							<swe:field name="sensor_id">
							</swe:field>
							<swe:field name="sensor_name">
							</swe:field>
							<swe:field name="phenomenon_name">
							</swe:field>
							<swe:field name="numeric_value">
							</swe:field>
							<swe:field name="phenomenon_unit">
							</swe:field>
							<swe:field name="latitude">
								<swe:Quantity definition="urn:ogc:def:crs:OGC:1.3:CRS84">
									<swe:uom xlink:href="urn:ogc:def:uom:OGC::deg" />
								</swe:Quantity>
							</swe:field>
							<swe:field name="longitude">
								<swe:Quantity definition="urn:ogc:def:crs:OGC:1.3:CRS83">
									<swe:uom xlink:href="urn:ogc:def:uom:OGC::deg" />
								</swe:Quantity>
							</swe:field>
							<swe:field name="time">
								<swe:Time definition="urn:ogc:def:phenomenon:time:iso8601" />
							</swe:field>
						</swe:DataRecord>
					</swe:elementType>
					<swe:encoding>
						<swe:TextBlock decimalSeparator='.' tokenSeparator='$_$' blockSeparator='@-@' />
					</swe:encoding>
					<swe:values>
						<xsl:for-each select="root/measures">
							<xsl:value-of select="sensor_id" />$_$<xsl:value-of select="sensor_name" />$_$<xsl:value-of select="phenomenon_name" />$_$<xsl:value-of select="value" />$_$<xsl:value-of select="unit_of_measurement"/>$_$<xsl:value-of select="geo_location/coordinates[1]" />$_$<xsl:value-of select="geo_location/coordinates[2]" />$_$<xsl:value-of select="time" />@-@
						</xsl:for-each>
					</swe:values>
				</swe:DataArray>
			</om:result>
		</om:OM_Observation>
	</xsl:template>
</xsl:stylesheet>
