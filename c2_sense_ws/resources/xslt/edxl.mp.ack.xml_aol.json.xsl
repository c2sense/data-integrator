<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg" 
	xmlns:b="urn:oasis:names:tc:emergency:EDXL:RM:1.0"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
	<xsl:template match="/">
		<AOLMessage>
			<MessageID><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "MessageID"]'/></MessageID>
			<ThreadID><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "ThreadID"]'/></ThreadID>
			<ReplyID><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "ReplyID"]'/></ReplyID>
			<From>
				<AOLNetworkID><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "From"]/*[local-name() = "AOLNetworkID"]'/></AOLNetworkID>
				<Name><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "From"]/*[local-name() = "Name"]'/></Name>
			</From>
			<xsl:for-each select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "To"]'>
				<To>
					<AOLNetworkID><xsl:value-of select='*[local-name() = "AOLNetworkID"]' /></AOLNetworkID>
					<Name>
						<xsl:value-of select='*[local-name() = "Name"]' />
					</Name>
				</To>
			</xsl:for-each>
			<xsl:for-each select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "CC"]'>
				<To>
					<AOLNetworkID><xsl:value-of select='*[local-name() = "AOLNetworkID"]' /></AOLNetworkID>
					<Name>
						<xsl:value-of select='*[local-name() = "Name"]' />
					</Name>
				</To>
			</xsl:for-each>
			<Subject><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "Subject"]'/></Subject>
			<Body><xsl:value-of select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "AOLMessage"]/*[local-name() = "Body"]'/></Body>
		</AOLMessage>
	</xsl:template>
</xsl:stylesheet>