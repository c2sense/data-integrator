<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" />
    <xsl:template match="/">
        <om:OM_Observation xmlns:om='http://www.opengis.net/om/2.0' xmlns:gml='http://www.opengis.net/gml/3.2' xmlns:swe='http://www.opengis.net/swe/1.0/gml32' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
            <xsl:attribute name="gml:id">O35235</xsl:attribute>
            <xsl:attribute name="xsi:schemaLocation">http://www.opengis.net/om/2.0 http://schemas.opengis.net/om/2.0/observation.xsd</xsl:attribute>
            <gml:name>
                <xsl:value-of select="om:OM_Observation/gml:name/text()" />
            </gml:name>
            <om:type>
                <xsl:attribute name="xlink:href">http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_SWEObservation</xsl:attribute>
            </om:type>
            <om:phenomenonTime>
                <gml:TimePeriod>
                    <xsl:attribute name="gml:id">
                        <xsl:value-of select="om:OM_Observation/om:phenomenonTime/gml:TimePeriod/@gml:id" />
                    </xsl:attribute>
                    <gml:beginPosition>
                        <xsl:value-of select="om:OM_Observation/om:phenomenonTime/gml:TimePeriod/gml:beginPosition/text()" />
                    </gml:beginPosition>
                    <gml:endPosition>
                        <xsl:value-of select="om:OM_Observation/om:phenomenonTime/gml:TimePeriod/gml:endPosition/text()" />
                    </gml:endPosition>
                </gml:TimePeriod>
            </om:phenomenonTime>
            <om:resultTime>
                <gml:TimeInstant>
                    <xsl:attribute name="gml:id">
                        <xsl:value-of select="om:OM_Observation/om:resultTime/gml:TimeInstant/@gml:id" />
                    </xsl:attribute>
                    <gml:timePosition>
                        <xsl:value-of select="om:OM_Observation/om:resultTime/gml:TimeInstant/gml:timePosition/text()" />
                    </gml:timePosition>
                </gml:TimeInstant>
            </om:resultTime>
            <om:procedure>
                <xsl:attribute name="xlink:href">
                    <xsl:value-of select="om:OM_Observation/om:procedure/@xlink:href" />
                </xsl:attribute>
            </om:procedure>
            <om:observedProperty>
                <xsl:attribute name="xlink:href">
                    <xsl:value-of select="om:OM_Observation/om:observedProperty/@xlink:href" />
                </xsl:attribute>
            </om:observedProperty>
            <om:featureOfInterest>
            	<xsl:attribute name="xlink:title">
                    <xsl:value-of select="om:OM_Observation/om:featureOfInterest/@xlink:title" />
                </xsl:attribute>
                <xsl:attribute name="xlink:href">
                    <xsl:value-of select="om:OM_Observation/om:featureOfInterest/@xlink:href" />
                </xsl:attribute>
            </om:featureOfInterest>
            <om:result>
                <swe:DataArray>
                    <swe:elementCount>
                        <swe:Count>
                            <swe:value>
                                <xsl:value-of select="om:OM_Observation/om:result/swe:DataArray/swe:elementCount/swe:Count/swe:value/text()" />
                            </swe:value>
                        </swe:Count>
                    </swe:elementCount>
                    <swe:elementType>
                        <xsl:attribute name="name">
                            <xsl:value-of select="om:OM_Observation/om:result/swe:DataArray/swe:elementType/@name" />
                        </xsl:attribute>
                        <swe:DataRecord xmlns:gml="http://www.opengis.net/gml" xmlns:swe="http://www.opengis.net/swe/1.0.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/swe/1.0.1 http://schemas.opengis.net/sweCommon/1.0.1/swe.xsd">
							<swe:field name="sensor_id">
							</swe:field>
							<swe:field name="sensor_name">
							</swe:field>
							<swe:field name="phenomenon_name">
							</swe:field>
							<swe:field name="numeric_value">
							</swe:field>
							<swe:field name="phenomenon_unit">
							</swe:field>
							<swe:field name="latitude">
								<swe:Quantity definition="urn:ogc:def:crs:OGC:1.3:CRS84">
									<swe:uom xlink:href="urn:ogc:def:uom:OGC::deg" />
								</swe:Quantity>
							</swe:field>
							<swe:field name="longitude">
								<swe:Quantity definition="urn:ogc:def:crs:OGC:1.3:CRS83">
									<swe:uom xlink:href="urn:ogc:def:uom:OGC::deg" />
								</swe:Quantity>
							</swe:field>
							<swe:field name="time">
								<swe:Time definition="urn:ogc:def:phenomenon:time:iso8601" />
							</swe:field>
						</swe:DataRecord>
                    </swe:elementType>
                    <swe:encoding>
                        <swe:TextBlock>
                            <xsl:attribute name="decimalSeparator">
                                <xsl:value-of select="om:OM_Observation/om:result/swe:DataArray/swe:encoding/swe:TextBlock/@decimalSeparator" />
                            </xsl:attribute>
                            <xsl:attribute name="tokenSeparator">
                                <xsl:value-of select="om:OM_Observation/om:result/swe:DataArray/swe:encoding/swe:TextBlock/@tokenSeparator" />
                            </xsl:attribute>
                            <xsl:attribute name="blockSeparator">
                                <xsl:value-of select="om:OM_Observation/om:result/swe:DataArray/swe:encoding/swe:TextBlock/@blockSeparator" />
                            </xsl:attribute>
                        </swe:TextBlock>
                    </swe:encoding>
                    <swe:values>
                        <xsl:value-of select="om:OM_Observation/om:result/swe:DataArray/swe:values/text()" />
                    </swe:values>
                </swe:DataArray>
            </om:result>
        </om:OM_Observation>
    </xsl:template>
</xsl:stylesheet>
