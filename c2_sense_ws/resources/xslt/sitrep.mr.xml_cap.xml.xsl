<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="urn:oasis:names:tc:emergency:cap:1.2" xmlns:b="urn:oasis:names:tc:emergency:EDXL:SitRep:1.0" xmlns:c="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xnl" xmlns:d="urn:oasis:names:tc:emergency:edxl:ct:1.0">
	<xsl:template match="/">
		<a:alert>
			<a:identifier>
				<xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "messageID"]'/>
			</a:identifier>
			<a:sender>
				<xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "preparedBy"]/*[local-name() = "personDetails"]/*[local-name() = "personName"]/*[local-name() = "nameElement"]'/>
			</a:sender>
			<a:sent>
				<xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "forTimePeriod"]/*[local-name() = "toDateTime"]'/>
			</a:sent>
			<a:status>System</a:status>
			<a:msgType>Alert</a:msgType>
			<a:scope>Public</a:scope>
			<a:info>
				<a:language>it-IT</a:language>
				<a:event>
					<xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "reportTitle"]'/>
				</a:event>
				<a:urgency>
					<xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "urgency"]'/>
				</a:urgency>
				<a:severity>
					<xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "severity"]'/>
				</a:severity>
				<a:certainty>Unknown</a:certainty>
				<a:senderName>
					<xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "preparedBy"]/*[local-name() = "personDetails"]/*[local-name() = "personName"]/*[local-name() = "nameElement"]'/>
				</a:senderName>
				<a:instruction>
					<xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "actionPlan"]'/>
				</a:instruction>
			</a:info>
		</a:alert>
	</xsl:template>
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2009. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios/>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="..\schemas\cap\cap_1.2.xsd" destSchemaRoot="alert" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no">
			<SourceSchema srcSchemaPath="..\schemas\sitrep\EDXLSitRep-v1.0.xsd" srcSchemaRoot="sitRep" AssociatedInstance="" loaderFunction="document" loaderFunctionUsesURI="no"/>
		</MapperInfo>
		<MapperBlockPosition>
			<template match="/"></template>
		</MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->