<?xml version='1.0' ?>
<!-- MS04 To AOL SOIR
 	 MS09 To AOL SOIR -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg" 
	xmlns:c="urn:oasis:names:tc:emergency:EDXL:RM:1.0"
	xmlns:xpil="urn:oasis:names:tc:ciq:xpil:3" 
	xmlns:xnl="urn:oasis:names:tc:ciq:xnl:3" 
	xmlns:xal="urn:oasis:names:tc:ciq:xal:3"
    xmlns:geo-oasis="urn:oasis:names:tc:emergency:EDXL:HAVE:1.0:geo-oasis" 
    xmlns:gml="http://www.opengis.net/gml"
	xmlns:data="xalan://util.C2SDIUtils"
	extension-element-prefixes="data">
	<xsl:template match="/">
		<xsl:variable name="sentDate" select="data:UTCDate()" />
		<xsl:variable name="bodyMessage" select="AOLMessage/Body" />
		<a:RequisitionResource>
			<a:MessageID><xsl:value-of select="AOLMessage/ThreadID" />:<xsl:value-of select="AOLMessage/MessageID" /></a:MessageID>
			<a:SentDateTime><xsl:value-of select="$sentDate" /></a:SentDateTime>
			<a:MessageContentType>RequisitionResource</a:MessageContentType>
			<a:OriginatingMessageID><xsl:value-of select="AOLMessage/ThreadID" /></a:OriginatingMessageID>
			<a:PrecedingMessageID><xsl:value-of select="AOLMessage/ReplyID" /></a:PrecedingMessageID>
			<a:IncidentInformation>
				<c:IncidentID><xsl:value-of select="AOLMessage/ThreadID" /></c:IncidentID>
				<c:IncidentDescription><xsl:value-of select="AOLMessage/Subject" /></c:IncidentDescription>
			</a:IncidentInformation>
			<a:Funding>
				<c:FundCode>C2SENSE</c:FundCode>
			</a:Funding>
			<a:ContactInformation>
				<c:ContactRole>Sender</c:ContactRole>
				<c:ContactLocation><c:LocationDescription><xsl:value-of select="AOLMessage/Geometry" /></c:LocationDescription></c:ContactLocation>
				<c:AdditionalContactInformation>
					<xpil:PartyName>
						<xnl:PersonName>
							<xnl:NameElement xnl:ElementType="FirstName"><xsl:value-of select="AOLMessage/From/Name" /></xnl:NameElement>
						</xnl:PersonName>
					</xpil:PartyName>
					<xpil:ElectronicAddressIdentifiers>
						<xpil:ElectronicAddressIdentifier><xsl:value-of select="AOLMessage/From/AOLNetworkID" />:<xsl:value-of select="AOLMessage/From/Name" /></xpil:ElectronicAddressIdentifier>
					</xpil:ElectronicAddressIdentifiers>
				</c:AdditionalContactInformation>
			</a:ContactInformation>
			<a:ResourceInformation>
				<a:ResourceInfoElementID><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' TypeStructure ')" /></a:ResourceInfoElementID>
				<a:Resource>
					<a:Description><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' Description ')" /></a:Description>
				</a:Resource>
				<a:AssignmentInformation>
					<a:Quantity>
						<c:MeasuredQuantity>
							<c:Amount><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' Quantity ')" /></c:Amount>
						</c:MeasuredQuantity>
					</a:Quantity>
				</a:AssignmentInformation>
			</a:ResourceInformation>
		</a:RequisitionResource>
	</xsl:template>
</xsl:stylesheet>