<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="urn:oasis:names:tc:emergency:cap:1.2" xmlns:b="urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg" xmlns:c="urn:oasis:names:tc:emergency:EDXL:RM:1.0">
<xsl:template match="/">
<alert>
	<identifier><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "MessageID"]'/></identifier>
	<!--
	<sender><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]'/></sender>
	According to what was agreed with Daniel from Safran change this to dummySender in order to be replaced by LAR
	-->
	<sender>dummySender</sender>
	<sent><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "SentDateTime"]'/></sent>
	<status>System</status>
	<msgType>Alert</msgType>
	<source>EDXL-RM ResourceRequest Conversion to CAP</source>
	<scope>Private</scope>
	<!-- 
	<addresses><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]'/></addresses>
	According to what was agreed with Daniel from Safran change this to dummyAddresses in order to be replaced by LAR
	-->
	<addresses>dummyAddresses</addresses>
	<code><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "MessageContentType"]'/></code>
	<note><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "MessageDescription"]'/></note>
	<!-- 
		FireBrigade will need this because incident id MUST be unique so I use ThreadId:MessageId that are coming from AOL previuos transformation
	 -->
	<incidents><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "MessageID"]'/></incidents>
	<info>
		<language>it-IT</language>
		<category>Other</category>
		<event><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "IncidentInformation"]/*[local-name() = "IncidentDescription"]'/></event>
		<urgency>Unknown</urgency>
		<severity>Unknown</severity>
		<certainty>Unknown</certainty>
		<senderName><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "PartyName"]/*[local-name() = "PersonName"]/*[local-name() = "NameElement"]'/></senderName>
		<contact><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]'/></contact>
		<parameter>
			<valueName>C2SENSE_MessageID</valueName>
			<value><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "MessageID"]'/></value>
		</parameter>
		<xsl:for-each select='*[local-name() = "RequestResource"]/*[local-name() = "ResourceInformation"]'>
		<parameter>
			<valueName>ResourceInfoElementID</valueName>
			<value><xsl:value-of select='*[local-name() = "ResourceInfoElementID"]'/></value>
		</parameter>
		<parameter>
			<valueName>Resource</valueName>
			<value><xsl:value-of select='*[local-name() = "Resource"]'/></value>
		</parameter>
		</xsl:for-each>
	</info>
</alert>
</xsl:template>
</xsl:stylesheet>