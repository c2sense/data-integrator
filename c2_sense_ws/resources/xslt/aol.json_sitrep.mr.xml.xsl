<?xml version="1.0" encoding="UTF-8"?>
<!-- MS04 To AOL COC 
	 MS06 To H2H CFD
 	 MS09 To AOL SOIR
 	 MS09 To H2H CCS -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ct="urn:oasis:names:tc:emergency:edxl:ct:1.0"
	xmlns:edxl-gsf="urn:oasis:names:tc:emergency:edxl:gsf:1.0" xmlns:ext="urn:oasis:names:tc:emergency:edxl:extension:1.0"
	xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:p="urn:oasis:names:tc:emergency:EDXL:SitRep:1.0"
	xmlns:p1="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xpil" xmlns:p2="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xnl"
	xmlns:p3="urn:oasis:names:tc:emergency:edxl:ciq:1.0:ct" xmlns:p4="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xal"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:data="xalan://util.C2SDIUtils"
	extension-element-prefixes="data">
	<xsl:template match="/">
		<xsl:variable name="sentDate" select="data:UTCDate()" />
		<xsl:variable name="bodyMessage" select="AOLMessage/Body" />
		<p:sitRep>
			<p:messageID><xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/></p:messageID>
			<p:preparedBy>
				<ct:personDetails>
					<p2:personName>
						<p2:nameElement><xsl:value-of select="AOLMessage/From/Name"/></p2:nameElement>
					</p2:personName>
				</ct:personDetails>
				<ct:timeValue><xsl:value-of select="$sentDate"/></ct:timeValue>
			</p:preparedBy>
			<p:authorizedBy>
				<ct:personDetails>
					<p2:personName>
						<p2:nameElement><xsl:value-of select="AOLMessage/From/Name"/></p2:nameElement>
					</p2:personName>
				</ct:personDetails>
				<ct:timeValue><xsl:value-of select="$sentDate"/></ct:timeValue>
			</p:authorizedBy>
			<p:reportPurpose><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' ReportPurpose ')" /></p:reportPurpose>
			<p:reportNumber>0</p:reportNumber>
			<p:reportVersion>Initial</p:reportVersion>
			<p:forTimePeriod>
				<ct:fromDateTime><xsl:value-of select="$sentDate"/></ct:fromDateTime>
				<ct:toDateTime><xsl:value-of select="$sentDate"/></ct:toDateTime>
			</p:forTimePeriod>
			<p:reportTitle><xsl:value-of select="AOLMessage/Subject"/></p:reportTitle>
			<p:incidentID><xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/></p:incidentID>
			<p:reportConfidence>Unsure</p:reportConfidence>
			<p:severity>Unknown</p:severity>
			<p:report xsi:type="p:ManagementReportingSummaryType">
				<p:situationSummary>
					<p:incidentCause><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' SituationSummary ')" /></p:incidentCause>
					<p:damageAssessmentInformation><xsl:value-of select="AOLMessage/Geometry"/></p:damageAssessmentInformation>
					<p:hazMatIncidentReport></p:hazMatIncidentReport>
					<p:lifeAndSafetyThreat>PotentialFutureThreat</p:lifeAndSafetyThreat>
				</p:situationSummary>
			</p:report>
		</p:sitRep>
	</xsl:template>
</xsl:stylesheet>