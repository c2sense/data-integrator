<?xml version="1.0" encoding="UTF-8"?>
<!-- MS05 To AOL SOIR -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ct="urn:oasis:names:tc:emergency:edxl:ct:1.0"
	xmlns:edxl-gsf="urn:oasis:names:tc:emergency:edxl:gsf:1.0" xmlns:ext="urn:oasis:names:tc:emergency:edxl:extension:1.0"
	xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:p="urn:oasis:names:tc:emergency:EDXL:SitRep:1.0"
	xmlns:p1="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xpil" xmlns:p2="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xnl"
	xmlns:p3="urn:oasis:names:tc:emergency:edxl:ciq:1.0:ct" xmlns:p4="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xal"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:data="xalan://util.C2SDIUtils"
	extension-element-prefixes="data">
	<xsl:template match="/">
		<xsl:variable name="sentDate" select="data:UTCDate()" />
		<xsl:variable name="bodyMessage" select="AOLMessage/Body" />
		<p:sitRep>
			<p:messageID><xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/></p:messageID>
			<p:preparedBy>
				<ct:personDetails>
					<p2:personName>
						<p2:nameElement><xsl:value-of select="AOLMessage/From/Name"/></p2:nameElement>
					</p2:personName>
				</ct:personDetails>
				<ct:timeValue><xsl:value-of select="$sentDate"/></ct:timeValue>
			</p:preparedBy>
			<p:authorizedBy>
				<ct:personDetails>
					<p2:personName>
						<p2:nameElement><xsl:value-of select="AOLMessage/From/Name"/></p2:nameElement>
					</p2:personName>
				</ct:personDetails>
				<ct:timeValue><xsl:value-of select="$sentDate"/></ct:timeValue>
			</p:authorizedBy>
			<p:reportPurpose><xsl:value-of select="AOLMessage/Body"/></p:reportPurpose>
			<p:reportNumber>0</p:reportNumber>
			<p:reportVersion>Initial</p:reportVersion>
			<p:forTimePeriod>
				<ct:fromDateTime><xsl:value-of select="$sentDate"/></ct:fromDateTime>
				<ct:toDateTime><xsl:value-of select="$sentDate"/></ct:toDateTime>
			</p:forTimePeriod>
			<p:reportTitle><xsl:value-of select="AOLMessage/Subject"/></p:reportTitle>
			<p:incidentID><xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/></p:incidentID>
			<p:reportConfidence>Unsure</p:reportConfidence>
			<p:severity>Unknown</p:severity>
			<p:report xsi:type="p:SituationInformationType">
				<p:primaryIncidentInformation>
					<p:incidentName><xsl:value-of select="AOLMessage/Subject"/></p:incidentName>
					<p:geographicSize>
						<p:size>1</p:size>
					</p:geographicSize>
					<p:incidentLocation>
						<ct:EDXLGeoPoliticalLocation>
							<ct:geoCode>
								<ct:valueListURI>aol:kml:geostring</ct:valueListURI>
								<ct:value><xsl:value-of select="AOLMessage/Geometry"/></ct:value>
							</ct:geoCode>
						</ct:EDXLGeoPoliticalLocation>
					</p:incidentLocation>
				</p:primaryIncidentInformation>
			</p:report>
		</p:sitRep>
	</xsl:template>
</xsl:stylesheet>