<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="urn:oasis:names:tc:emergency:cap:1.2"
	xmlns:b="urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg" xmlns:c="urn:oasis:names:tc:emergency:EDXL:RM:1.0">
	<xsl:template match="/">
		<alert>
			<identifier><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "MessageID"]' /></identifier>
			<!--
			<sender><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]' /></sender> 
			According to what was agreed with Daniel from Safran change this to dummySender in order to be replaced by LAR
			-->
			<sender>dummySender</sender>
			<sent><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "SentDateTime"]' /></sent>
			<status>System</status>
			<msgType>Alert</msgType>
			<source>EDXL-RM RequisitionResource Conversion to CAP</source>
			<scope>Private</scope>
			<!-- 
			<addresses><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]' /></addresses>
			According to what was agreed with Daniel from Safran change this to dummyAddresses in order to be replaced by LAR
			-->
			<addresses>dummyAddresses</addresses>			
			<code><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "MessageContentType"]' /></code>
			<note><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "MessageDescription"]' /></note>
			<incidents><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "MessageID"]' /></incidents>
			<info>
				<language>it-IT</language>
				<category>Other</category>
				<event><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "IncidentInformation"]/*[local-name() = "IncidentDescription"]' /></event>
				<urgency>Unknown</urgency>
				<severity>Unknown</severity>
				<certainty>Unknown</certainty>
				<senderName><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "PartyName"]/*[local-name() = "PersonName"]/*[local-name() = "NameElement"]'/></senderName>
				<contact><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]' /></contact>
				<xsl:for-each select='*[local-name() = "RequisitionResource"]/*[local-name() = "ResourceInformation"]'>
					<parameter>
						<valueName>ResourceInfoElementID</valueName>
						<value><xsl:value-of select='*[local-name() = "ResourceInfoElementID"]' /></value>
					</parameter>
					<parameter>
						<valueName>Resource</valueName>
						<value><xsl:value-of select='*[local-name() = "Resource"]/*[local-name() = "Description"]' /></value>
					</parameter>
					<parameter>
						<valueName>Quantity</valueName>
						<value><xsl:value-of select='*[local-name() = "AssignmentInformation"]/*[local-name() = "Quantity"]/*[local-name() = "MeasuredQuantity"]/*[local-name() = "Amount"]'/></value>
					</parameter>
				</xsl:for-each>
				<parameter>
					<valueName>C2SENSE_MessageID</valueName>
					<value><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "MessageID"]'/></value>
				</parameter>
				<parameter>
					<valueName>C2SENSE_PrecedingMessageID</valueName>
					<value><xsl:value-of select='*[local-name() = "RequisitionResource"]/*[local-name() = "PrecedingMessageID"]'/></value>
				</parameter>
			</info>
		</alert>
	</xsl:template>
</xsl:stylesheet>