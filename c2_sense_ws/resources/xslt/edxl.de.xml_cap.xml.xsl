<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="urn:oasis:names:tc:emergency:cap:1.2">

	<xsl:template match='/'>
		<xsl:apply-templates select='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "alert"]'/>
	</xsl:template>

	<xsl:template match='*[local-name() = "edxlDistribution"]/*[local-name() = "content"]/*[local-name() = "contentObject"]/*[local-name() = "contentXML"]/*[local-name() = "embeddedXMLContent"]/*[local-name() = "alert"]'>
		<a:alert>
			<a:identifier><xsl:value-of select='*[local-name()="identifier"]' /></a:identifier>
			<a:sender><xsl:value-of select='*[local-name()="sender"]' /></a:sender>
			<a:sent><xsl:value-of select='*[local-name()="sent"]' /></a:sent>
			<a:status><xsl:value-of select='*[local-name()="status"]' /></a:status>
			<a:msgType><xsl:value-of select='*[local-name()="msgType"]' /></a:msgType>
			<a:source><xsl:value-of select='*[local-name()="source"]' /></a:source>
			<a:scope><xsl:value-of select='*[local-name()="scope"]' /></a:scope>
			<a:restriction><xsl:value-of select='*[local-name()="restriction"]' /></a:restriction>
			<a:addresses><xsl:value-of select='*[local-name()="addresses"]' /></a:addresses>
			<a:code><xsl:value-of select='*[local-name()="code"]' /></a:code>
			<a:note><xsl:value-of select='*[local-name()="note"]' /></a:note>
			<xsl:if test='*[local-name()="references"] != ""'>
			<a:references><xsl:value-of select='*[local-name()="references"]' /></a:references>
			</xsl:if>
			<a:incidents><xsl:value-of select='*[local-name()="incidents"]' /></a:incidents>
			<xsl:for-each select='*[local-name()="info"]'>
				<a:info>
					<a:language><xsl:value-of select='*[local-name()="language"]' /></a:language>
					<xsl:for-each select='*[local-name()="category"]'>
						<a:category><xsl:value-of select='.' /></a:category>
					</xsl:for-each>
					<a:event><xsl:value-of select='*[local-name()="event"]' /></a:event>
					<xsl:for-each select='*[local-name()="responseType"]'>
						<a:responseType><xsl:value-of select='.' /></a:responseType>
					</xsl:for-each>
					<a:urgency><xsl:value-of select='*[local-name()="urgency"]' /></a:urgency>
					<a:severity><xsl:value-of select='*[local-name()="severity"]' /></a:severity>
					<a:certainty><xsl:value-of select='*[local-name()="certainty"]' /></a:certainty>
					<a:audience><xsl:value-of select='*[local-name()="audience"]' /></a:audience>
					<xsl:for-each select='*[local-name()="eventCode"]'>
						<a:eventCode>
							<a:valueName><xsl:value-of select='*[local-name()="valueName"]' /></a:valueName>
							<a:value><xsl:value-of select='*[local-name()="value"]' /></a:value>
						</a:eventCode>
					</xsl:for-each>
					<xsl:if test='*[local-name()="effective"]'>
					<a:effective><xsl:value-of select='*[local-name()="effective"]' /></a:effective>
					</xsl:if>
					<xsl:if test='*[local-name()="onset"]'>
						<a:onset><xsl:value-of select='*[local-name()="onset"]' /></a:onset>
					</xsl:if>
					<xsl:if test='*[local-name()="expires"]'>
					<a:expires><xsl:value-of select='*[local-name()="expires"]' /></a:expires>
					</xsl:if>
					<a:senderName><xsl:value-of select='*[local-name()="senderName"]' /></a:senderName>
					<a:headline><xsl:value-of select='*[local-name()="headline"]' /></a:headline>
					<a:description><xsl:value-of select='*[local-name()="description"]' /></a:description>
					<a:instruction><xsl:value-of select='*[local-name()="instruction"]' /></a:instruction>
					<xsl:if test='*[local-name()="web"] != ""'>
					<a:web><xsl:value-of select='*[local-name()="web"]' /></a:web>
					</xsl:if>
					<a:contact><xsl:value-of select='*[local-name()="contact"]' /></a:contact>
					<xsl:for-each select='*[local-name()="parameter"]'>
						<a:parameter><xsl:value-of select='*[local-name()="parameter"]' />
							<a:valueName><xsl:value-of select='*[local-name()="valueName"]' /></a:valueName>
							<a:value><xsl:value-of select='*[local-name()="value"]' /></a:value>
						</a:parameter>
					</xsl:for-each>
					<xsl:for-each select='*[local-name()="resource"]'>
						<a:resource>
							<a:resourceDesc><xsl:value-of select='*[local-name()="resourceDesc"]'/></a:resourceDesc>
							<a:mimeType><xsl:value-of select='*[local-name()="mimeType"]'/></a:mimeType>
							<a:size><xsl:value-of select='*[local-name()="size"]'/></a:size>
							<a:uri><xsl:value-of select='*[local-name()="uri"]'/></a:uri>
							<a:derefUri><xsl:value-of select='*[local-name()="derefUri"]'/></a:derefUri>
							<a:digest><xsl:value-of select='*[local-name()="digest"]'/></a:digest>
						</a:resource>
					</xsl:for-each>
					<xsl:for-each select='*[local-name()="area"]'>
						<a:area>
							<a:areaDesc><xsl:value-of select='*[local-name()="areaDesc"]' /></a:areaDesc>
							<xsl:for-each select='*[local-name()="polygon"]'>
								<a:polygon><xsl:value-of select='.' /></a:polygon>
							</xsl:for-each>
							<xsl:for-each select='*[local-name()="circle"]'>
								<a:circle><xsl:value-of select='.' /></a:circle>
							</xsl:for-each>
							<xsl:for-each select='*[local-name()="geocode"]'>
								<a:geocode>
									<a:valueName><xsl:value-of select='*[local-name()="valueName"]' /></a:valueName>
	  								<a:value><xsl:value-of select='*[local-name()="value"]' /></a:value>
	  							</a:geocode>
							</xsl:for-each>
							<xsl:if test='*[local-name()="altitude"]'>
							<a:altitude><xsl:value-of select='*[local-name()="altitude"]' /></a:altitude>
							</xsl:if>
							<xsl:if test='*[local-name()="ceiling"]'>
							<a:ceiling><xsl:value-of select='*[local-name()="ceiling"]' /></a:ceiling>
							</xsl:if>
						</a:area>
					</xsl:for-each>
				</a:info>
			</xsl:for-each>
		</a:alert>
	</xsl:template>
</xsl:stylesheet>