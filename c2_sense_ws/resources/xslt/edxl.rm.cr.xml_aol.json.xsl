<?xml version='1.0' ?>
<!-- MS04 To AOL SOIR
 	 MS09 To AOL SOIR -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg" 
	xmlns:b="urn:oasis:names:tc:emergency:EDXL:RM:1.0"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
	<xsl:template match="/">
		<xsl:variable name="AolIds" select='*[local-name() = "CommitResource"]/*[local-name() = "MessageID"]' />
		<AOLMessage>
			<MessageID><xsl:value-of select="data:splitString($AolIds, ':', 1)" /></MessageID>
			<ThreadID><xsl:value-of select="data:splitString($AolIds, ':', 0)" /></ThreadID>
			<ReplyID><xsl:value-of select='*[local-name() = "CommitResource"]/*[local-name() = "PrecedingMessageID"]'/></ReplyID>
			<From>
				<AOLNetworkID><xsl:value-of select='*[local-name() = "CommitResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "ContactDescription"]'/></AOLNetworkID>
				<Name><xsl:value-of select='*[local-name() = "CommitResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "ContactDescription"]'/></Name>
			</From>
			<To>
				<AOLNetworkID>TO</AOLNetworkID>
				<Name>TO</Name>
			</To>
			<Subject><xsl:value-of select='*[local-name() = "CommitResource"]/*[local-name() = "IncidentInformation"]/*[local-name() = "IncidentDescription"]'/></Subject>
			<Body><![CDATA[<HTML><BODY><P><SPAN>== TypeStructure ==</SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "CommitResource"]/*[local-name() = "ResourceInformation"]/*[local-name() = "ResourceInfoElementID"]' /><![CDATA[</SPAN></P><P><SPAN>== Description == </SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "CommitResource"]/*[local-name() = "ResourceInformation"]/*[local-name() = "Resource"]/*[local-name() = "Description"]' /><![CDATA[</SPAN></P> <P><SPAN>== ResponseType == </SPAN></P>  <P><SPAN>]]><xsl:value-of select='*[local-name() = "CommitResource"]/*[local-name() = "ResourceInformation"]/*[local-name() = "ResponseInformation"]/*[local-name() = "ResponseType"]' /><![CDATA[</SPAN></P></BODY></HTML>]]></Body>
			<xsl:if test='*[local-name() = "CommitResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "ContactLocation"] != ""'>
			<Geometry><xsl:value-of select='*[local-name() = "CommitResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "ContactLocation"]'/></Geometry>
			</xsl:if>
		</AOLMessage>
	</xsl:template>
</xsl:stylesheet>