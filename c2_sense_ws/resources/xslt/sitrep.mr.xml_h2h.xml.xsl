<?xml version='1.0' ?>
 <!-- MS06 To H2H CFD
 	  MS09 To H2H CCS -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HtH_Message>
			<HtH_Content>
Report Purpose: <xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "reportPurpose"]'/>
Situation Summary: <xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "report"]/*[local-name() = "situationSummary"]/*[local-name() = "incidentCause"]'/>
	</HtH_Content>
	<HtH_Property>
		<HtH_PropertyKey>Subject</HtH_PropertyKey>
		<HtH_PropertyValue><xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "reportTitle"]'/></HtH_PropertyValue>
	</HtH_Property>
	<HtH_Property>
		<HtH_PropertyKey>Format</HtH_PropertyKey>
		<HtH_PropertyValue>text/plain</HtH_PropertyValue>
	</HtH_Property>
</HtH_Message>
	</xsl:template>
</xsl:stylesheet>