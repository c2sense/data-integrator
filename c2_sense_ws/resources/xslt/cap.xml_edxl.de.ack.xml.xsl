<?xml version='1.0' ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:b="urn:oasis:names:tc:emergency:cap:1.2" xmlns:a="urn:oasis:names:tc:emergency:EDXL:DE:2.0">
	<xsl:template match="/">
		<a:edxlDistribution>
			<a:distributionID>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "identifier"]'/>
			</a:distributionID>
			<a:senderID>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sender"]'/>
			</a:senderID>
			<a:dateTimeSent>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sent"]'/>
			</a:dateTimeSent>
			<a:dateTimeExpires>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sent"]'/>
			</a:dateTimeExpires>
			<a:distributionStatus>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "status"]'/>
			</a:distributionStatus>
			<a:distributionKind>Ack</a:distributionKind>
		</a:edxlDistribution>
	</xsl:template>
</xsl:stylesheet>