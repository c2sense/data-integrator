<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<!-- MS03 to MCP 
	 MS08 To MCP, H2H to Citizens 
 	 MS10 To AOL SOIR
 	 MS10 To FireBrigade
 	 MS10 To EMT SOIR -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="urn:oasis:names:tc:emergency:cap:1.2" xmlns:data="xalan://util.C2SDIUtils"
	extension-element-prefixes="data">
	<xsl:template match="/">
		<xsl:variable name="sentDate" select="data:UTCDate()" />
		<xsl:variable name="bodyMessage" select="AOLMessage/Body" />
		<alert>
			<identifier><xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/></identifier>
			<!-- 
				Change this on 13 april 2017 because the MS10 changed so we need to use AOL_CAP transformation. Reporting the requests made by Daniel for CAP message that is sent to FireBrigade
				<sender>"<xsl:value-of select="AOLMessage/From/AOLNetworkID"/>:<xsl:value-of select="AOLMessage/From/Name"/>"</sender>
			 -->
			<sender>dummySender</sender>
			<sent><xsl:value-of select="$sentDate" /></sent>
			<status>System</status>
			<msgType>Alert</msgType>
			<source>AOL</source>
			<scope>Private</scope>
			<!-- 
			Change this on 13 april 2017 because the MS10 changed so we need to use AOL_CAP transformation. Reporting the requests made by Daniel for CAP message that is sent to FireBrigade
			<addresses><xsl:for-each select="AOLMessage/To">"<xsl:value-of select="AOLNetworkID"/>:<xsl:value-of select="Name"/>" </xsl:for-each> <xsl:for-each select="AOLMessage/CC">"<xsl:value-of select="AOLNetworkID"/>:<xsl:value-of select="Name"/>" </xsl:for-each> </addresses>
			 -->
			<addresses>dummyAddresses</addresses>
			<!-- 
			Change this on 13 april 2017 because the MS10 changed so we need to use AOL_CAP transformation. now I'll use threadId:messageId from AOL
			<incidents>"<xsl:value-of select="AOLMessage/From/AOLNetworkID"/>@<xsl:value-of select="AOLMessage/From/Name"/>",<xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/>,<xsl:value-of select="$sentDate" /></incidents>
			-->
			<incidents><xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/></incidents>
			<info>
				<language>it-IT</language>
				<category>Infra</category>
				<event><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' EventType ')" /></event>
				<urgency>Unknown</urgency>
				<severity>Unknown</severity>
				<certainty>Unknown</certainty>
				<senderName><xsl:value-of select="AOLMessage/From/Name"/></senderName>
				<headline><xsl:value-of select="AOLMessage/Subject"/></headline>
				<description><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' Description ')" /></description>
				<xsl:for-each select="AOLMessage/Attachments">
				<resource>
					<resourceDesc><xsl:value-of select="Name"/></resourceDesc>
					<mimeType>application/pdf</mimeType>
					<size><xsl:value-of select="Size"/></size>
					<uri><xsl:value-of select="AttachID"/></uri>
					<derefUri>L0dldEF0dGFjaENvbnRlbnQ=</derefUri>
				</resource>
				</xsl:for-each>
				<xsl:if test="AOLMessage/Geometry">
				<xsl:variable name="geometry"><xsl:value-of select="AOLMessage/Geometry" /></xsl:variable>
				<area>
					<areaDesc>WKT String from ActOnline: <xsl:value-of select="AOLMessage/Geometry"/></areaDesc>
					<geocode>
						<valueName>WKT string</valueName>
						<value><xsl:value-of select="AOLMessage/Geometry"/></value>
					</geocode>
				</area>
				<area>
					<areaDesc>POINT</areaDesc>
					<polygon><xsl:value-of select="data:generatePolygonFromAOLGeometry($geometry)"/></polygon>
				</area>
				</xsl:if>
			</info>
		</alert>
	</xsl:template>
</xsl:stylesheet>