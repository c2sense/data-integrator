<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:b="urn:oasis:names:tc:emergency:cap:1.2"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data"
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:DE:2.0">
	<xsl:template match="/">
		<xsl:variable name="sentDate" select="data:UTCDate()" />
		<a:edxlDistribution xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns="urn:oasis:names:tc:emergency:EDXL:DE:2.0" xmlns:edxl-gsf="urn:oasis:names:tc:emergency:edxl:gsf:1.0"
			xmlns:ct="urn:oasis:names:tc:emergency:edxl:ct:1.0" xmlns:gml="http://www.opengis.net/gml/3.2"
			xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="extended">
			<a:distributionID>
				<xsl:value-of select="AOLACKMessage/MessageID" />
			</a:distributionID>
			<a:senderID>
				<xsl:value-of select="AOLACKMessage/MessageID" />@<xsl:value-of select="AOLACKMessage/From/Name" />
			</a:senderID>
			<a:dateTimeSent>
				<xsl:value-of select="$sentDate" />
			</a:dateTimeSent>
			<a:dateTimeExpires>
				<xsl:value-of select="$sentDate" />
			</a:dateTimeExpires>
			<a:distributionStatus>Exercise</a:distributionStatus>
			<a:distributionKind>Ack</a:distributionKind>
			<a:content xlink:type="resource">
				<a:contentObject xlink:type="resource">
					<a:contentDescriptor>
						<a:contentDescription>
							AOL Message for acknowledgement
						</a:contentDescription>
					</a:contentDescriptor>
					<a:contentXML>
						<a:embeddedXMLContent>
							<AOLACKMessage xmlns="aol:1.3 ../aol.xsd">
							    <MessageID><xsl:value-of select="AOLACKMessage/MessageID" /></MessageID>
							    <From>
							        <AOLNetworkID><xsl:value-of select="AOLACKMessage/From/AOLNetworkID" /></AOLNetworkID>
							        <Name><xsl:value-of select="AOLACKMessage/From/Name" /></Name>
							    </From>
							    <Status><xsl:value-of select="AOLACKMessage/Status" /></Status>
							</AOLACKMessage>
						</a:embeddedXMLContent>
					</a:contentXML>
				</a:contentObject>
			</a:content>
		</a:edxlDistribution>
	</xsl:template>
</xsl:stylesheet>