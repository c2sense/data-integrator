<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!-- MS02 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data" >
    <xsl:output encoding="UTF-8" method="xml" indent="yes" />
    <xsl:template match="/">
    	<xsl:variable name="signaling"><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "CommandAndSignal"]/*[local-name() = "Signaling"]'/></xsl:variable>
    	<xsl:variable name="execution"><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Execution"]/*[local-name() = "ConceptOfOperations"]'/></xsl:variable>
        <MsgSmsSend>
			<IdReceiver>dummyRecipient</IdReceiver>
			<IdChannel>dummyChannel</IdChannel>
			<Text><xsl:value-of select='data:cutMessageLength($signaling, $execution)'/></Text>
			<IdCommand>NULLSTRING</IdCommand>
			<Type>1</Type>
		</MsgSmsSend>
    </xsl:template>
</xsl:stylesheet>
