<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!-- MS07 From EMT To AOL COC 
 	  MS10 From COC To AOL SOIR -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data" xmlns:c="urn:oasis:names:tc:emergency:cap:1.2">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" />
    <xsl:template match="/">
    	<xsl:for-each select="c:alert/c:info">
    		<xsl:choose>
    			<xsl:when test="c:language = 'it-IT'">
					<xsl:variable name="AolIds" select='/c:alert/c:identifier' />
					<AOLMessage>
						<MessageID><xsl:value-of select="data:splitString($AolIds, ':', 1)" /></MessageID>
	                    <ThreadID><xsl:value-of select="data:splitString($AolIds, ':', 0)" /></ThreadID>
	                    <From>
	                        <AOLNetworkID>FROM</AOLNetworkID>
	                        <Name>FROM</Name>
	                    </From>
	                    <To>
	                        <AOLNetworkID>TO</AOLNetworkID>
	                        <Name>TO</Name>
	                    </To>
	                    <Subject><xsl:value-of select='c:headline' /></Subject>
	                    <Body><![CDATA[<HTML><BODY><P STYLE=\"margin:0 0 0 0;text-align:Left;font-family:Arial;font-size:14.6666666666667;\"><SPAN> == EventType == </SPAN></P><P STYLE=\"margin:0 0 0 0;text-align:Left;font-family:Arial;font-size:14.6666666666667;\"><SPAN>]]><xsl:value-of select='c:event' /><![CDATA[</SPAN></P><P STYLE=\"margin:0 0 0 0;text-align:Left;font-family:Arial;font-size:14.6666666666667;\"><SPAN> == Description == </SPAN></P><P STYLE=\"margin:0 0 0 0;text-align:Left;font-family:Arial;font-size:14.6666666666667;\"><SPAN>]]><xsl:value-of select='c:description' /><![CDATA[</SPAN></P></BODY></HTML>]]></Body>
	                    <xsl:choose>
	                        <xsl:when test="c:area/c:circle">
	                            <xsl:variable name="areaValue" select="c:area/c:circle" />
	                            <Geometry>
	                                <xsl:value-of select='data:generatePoint($areaValue)' />
	                            </Geometry>
	                        </xsl:when>
	                        <xsl:otherwise>
	                            <xsl:variable name="areaValue" select="c:area/c:polygon" />
	                            <Geometry>
	                                <xsl:value-of select='data:generatePoint($areaValue)' />
	                            </Geometry>
	                        </xsl:otherwise>
	                    </xsl:choose>
	                </AOLMessage>
    			</xsl:when>
    			<xsl:when test="c:language = 'en'">
    				<!-- CAP FROM EMT! -->
	                <xsl:variable name="aolId"><xsl:value-of select="data:generateAOLId()" /></xsl:variable>
	                <AOLMessage>
	                    <MessageID>
	                        <xsl:value-of select="$aolId" />
	                    </MessageID>
	                    <ThreadID>
	                        <xsl:value-of select="$aolId" />
	                    </ThreadID>
	                    <From>
	                        <AOLNetworkID>FROM</AOLNetworkID>
	                        <Name>FROM</Name>
	                    </From>
	                    <To>
	                        <AOLNetworkID>TO</AOLNetworkID>
	                        <Name>TO</Name>
	                    </To>
	                    <Subject>[Alert] Road Block Alert</Subject>
	                    <xsl:variable name="event"><xsl:value-of select="c:event" /></xsl:variable>
	                    <Body><![CDATA[<HTML><BODY><P><SPAN>]]><xsl:value-of select='data:extractMessageFromEMTCap($event)' /><![CDATA[</SPAN></P></BODY></HTML>]]></Body>
	                    <xsl:choose>
	                        <xsl:when test="c:area/c:circle">
	                            <xsl:variable name="areaValue" select="c:area/c:circle" />
	                            <Geometry>
	                                <xsl:value-of select='data:generatePoint($areaValue)' />
	                            </Geometry>
	                        </xsl:when>
	                        <xsl:otherwise>
	                            <xsl:variable name="areaValue" select="c:area/c:polygon" />
	                            <Geometry>
	                                <xsl:value-of select='data:generatePoint($areaValue)' />
	                            </Geometry>
	                        </xsl:otherwise>
	                    </xsl:choose>
	                </AOLMessage>
    			</xsl:when>
    			<xsl:otherwise>
    				<!-- CAP FROM EMT! -->
	                <xsl:variable name="aolId"><xsl:value-of select="data:generateAOLId()" /></xsl:variable>
	                <AOLMessage>
	                    <MessageID>
	                        <xsl:value-of select="$aolId" />
	                    </MessageID>
	                    <ThreadID>
	                        <xsl:value-of select="$aolId" />
	                    </ThreadID>
	                    <From>
	                        <AOLNetworkID>FROM</AOLNetworkID>
	                        <Name>FROM</Name>
	                    </From>
	                    <To>
	                        <AOLNetworkID>TO</AOLNetworkID>
	                        <Name>TO</Name>
	                    </To>
	                    <Subject>[Alert] Road Block Alert</Subject>
	                    <xsl:variable name="event"><xsl:value-of select="c:event" /></xsl:variable>
	                    <Body><![CDATA[<HTML><BODY><P><SPAN>]]><xsl:value-of select='data:extractMessageFromEMTCap($event)' /><![CDATA[</SPAN></P></BODY></HTML>]]></Body>
	                    <xsl:choose>
	                        <xsl:when test="c:area/c:circle">
	                            <xsl:variable name="areaValue" select="c:area/c:circle" />
	                            <Geometry>
	                                <xsl:value-of select='data:generatePoint($areaValue)' />
	                            </Geometry>
	                        </xsl:when>
	                        <xsl:otherwise>
	                            <xsl:variable name="areaValue" select="c:area/c:polygon" />
	                            <Geometry>
	                                <xsl:value-of select='data:generatePoint($areaValue)' />
	                            </Geometry>
	                        </xsl:otherwise>
	                    </xsl:choose>
	                </AOLMessage>
    			</xsl:otherwise>
    		</xsl:choose>
    	</xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
