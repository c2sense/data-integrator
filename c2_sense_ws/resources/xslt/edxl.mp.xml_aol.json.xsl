<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!-- MS03 From AOL 
     MS04 To AOL SOIR -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" />
    <xsl:template match="/">
	    <xsl:variable name="AolIds" select='*[local-name() = "MissionPlan"]/*[local-name() = "MessageID"]' />
        <AOLMessage>
			<MessageID><xsl:value-of select="data:splitString($AolIds, ':', 1)" /></MessageID>
			<ThreadID><xsl:value-of select="data:splitString($AolIds, ':', 0)" /></ThreadID>
			<xsl:if test="data:splitString($AolIds, ':', 2) !=''">
				<ReplyID><xsl:value-of select="data:splitString($AolIds, ':', 2)" /></ReplyID>
			</xsl:if>
			<From>
				<AOLNetworkID>1</AOLNetworkID>
				<Name><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "PreparedBy"]/*[local-name() = "PreparedBy"]/*[local-name() = "PersonDetails"]/*[local-name() = "PersonName"]/*[local-name() = "NameElement"]'/></Name>
			</From>
			<To>
				<AOLNetworkID>TO</AOLNetworkID>
				<Name>TO</Name>
			</To>
			<Subject><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Mission"]/*[local-name() = "Summary"]'/></Subject>
			<Body><![CDATA[<HTML><BODY><P><SPAN>== Threats ==</SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Threats"]/*[local-name() = "Description"]'/><![CDATA[</SPAN></P><P><SPAN>== Assets == </SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Assets"]/*[local-name() = "Description"]'/><![CDATA[</SPAN></P><P><SPAN>== Execution ==</SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Execution"]/*[local-name() = "ConceptOfOperations"]'/><![CDATA[</SPAN></P><P><SPAN>== CommandAndSignal ==</SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "CommandAndSignal"]/*[local-name() = "Signaling"]'/><![CDATA[</SPAN></P></BODY></HTML>]]></Body>
			<xsl:if test='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() ="EDXLGeoPoliticalLocation"]/*[local-name() ="GeoCode"]/*[local-name() ="Value"] != ""'>
				<Geometry><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() ="EDXLGeoPoliticalLocation"]/*[local-name() ="GeoCode"]/*[local-name() ="Value"]'/></Geometry>
			</xsl:if>
		</AOLMessage>
    </xsl:template>
</xsl:stylesheet>
