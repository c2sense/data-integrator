<?xml version="1.0" encoding="UTF-8"?>
<!-- MS03 to AOL 
	 MS04 To AOL SOIR -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ct="urn:oasis:names:tc:emergency:edxl:ct:1.0"
	xmlns:edxl-gsf="urn:oasis:names:tc:emergency:edxl:gsf:1.0" xmlns:gml="http://www.opengis.net/gml/3.2"
	xmlns:p="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xpil" xmlns:p1="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xnl"
	xmlns:p2="urn:oasis:names:tc:emergency:edxl:ciq:1.0:ct" xmlns:p3="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xal"
	xmlns:tns="http://www.c2sense.eu/edxl-mp" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data"
	xsi:schemaLocation="http://www.c2sense.eu/edxl-mp ../schemas/edxl.mp.xml.xsd ">
	<xsl:template match="/">
		<xsl:variable name="sentDate" select="data:UTCDate()" />
		<xsl:variable name="bodyMessage" select="AOLMessage/Body" />
		<tns:MissionPlan>
			<xsl:choose>
				<xsl:when test="AOLMessage/ReplyID != ''">
		        	<tns:MessageID><xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/>:<xsl:value-of select="AOLMessage/ReplyID"/></tns:MessageID>
		        </xsl:when>
		        <xsl:otherwise>
		        	<tns:MessageID><xsl:value-of select="AOLMessage/ThreadID"/>:<xsl:value-of select="AOLMessage/MessageID"/></tns:MessageID>
		        </xsl:otherwise>
			</xsl:choose>
			<tns:PlanID><xsl:value-of select="data:generateUUID()" /></tns:PlanID>
			<tns:PlanVersion>1</tns:PlanVersion>
			<tns:PreparedBy>
				<ct:PersonDetails>
					<p1:PersonName >
						<p1:NameElement><xsl:value-of select="AOLMessage/From/Name"/></p1:NameElement>
					</p1:PersonName>
				</ct:PersonDetails>
				<ct:TimeValue><xsl:value-of select="$sentDate" /></ct:TimeValue>
			</tns:PreparedBy>
			<tns:AuthorizedBy>
				<ct:PersonDetails>
					<p1:PersonName >
						<p1:NameElement><xsl:value-of select="AOLMessage/From/Name"/></p1:NameElement>
					</p1:PersonName>
				</ct:PersonDetails>
				<ct:TimeValue><xsl:value-of select="$sentDate" /></ct:TimeValue>
			</tns:AuthorizedBy>
			<tns:Situation>
				<tns:Field>
					<ct:EDXLGeoPoliticalLocation>
						<ct:GeoCode>
							<ct:ValueListURI>aol:kml:geostring</ct:ValueListURI>
							<ct:Value><xsl:value-of select="AOLMessage/Geometry"/></ct:Value>
						</ct:GeoCode>
					</ct:EDXLGeoPoliticalLocation>
				</tns:Field>
				<tns:Threats>
					<tns:Description><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' Threats ')" /></tns:Description>
				</tns:Threats>
				<tns:Assets>
					<tns:Description><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' Assets ')" /></tns:Description>
				</tns:Assets>
			</tns:Situation>
			<tns:Mission>
				<tns:Summary><xsl:value-of select="AOLMessage/Subject"/></tns:Summary>
			</tns:Mission>
			<tns:Execution>
				<tns:ConceptOfOperations><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' Execution ')" /></tns:ConceptOfOperations>
			</tns:Execution>
			<tns:CommandAndSignal>
				<tns:Signaling><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' CommandAndSignal ')" /></tns:Signaling>
			</tns:CommandAndSignal>
		</tns:MissionPlan>
	</xsl:template>
</xsl:stylesheet>