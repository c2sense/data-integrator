<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<edxlDistribution  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="urn:oasis:names:tc:emergency:EDXL:DE:2.0" xmlns:edxl-gsf="urn:oasis:names:tc:emergency:edxl:gsf:1.0" xmlns:ct="urn:oasis:names:tc:emergency:edxl:ct:1.0" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="extended" xsi:schemaLocation="urn:oasis:names:tc:emergency:EDXL:DE:2.0 ../schema/edxl-de-v2.0-wd11.xsd http://www.w3.org/2000/09/xmldsig# ./example-supporting-schema/xmldsig-core-schema.xsd http://www.w3.org/2001/04/xmlenc ./example-supporting-schema/xenc-schema.xsd">
			<distributionID>
				<xsl:value-of
					select='*[local-name() = "alert"]/*[local-name() = "identifier"]' />
			</distributionID>
			<senderID>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sender"]' />
			</senderID>
			<dateTimeSent>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sent"]' />
			</dateTimeSent>
			<distributionStatus>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "status"]' />
			</distributionStatus>
			<distributionKind>
				<xsl:value-of
					select='*[local-name() = "alert"]/*[local-name() = "msgType"]' />
			</distributionKind>
			<content>
				<contentObject>
					<contentDescriptor>
						<contentDescription>
							CAP MESSAGE CONVERTED TO HUMAN_TO_HUMAN
						</contentDescription>
					</contentDescriptor>
					<contentXML>
						<embeddedXMLContent>
							<HtH_Message>
								<HtH_Content>
<xsl:for-each select='*[local-name() = "alert"]/*[local-name() = "info"]' >
CAP INFO CONTENT:
language - <xsl:value-of select='*[local-name() = "language"]' />
<xsl:for-each select='*[local-name() = "category"]' >
category - <xsl:value-of select='.' />
</xsl:for-each>
event - <xsl:value-of select='*[local-name() = "event"]' />
responseType - <xsl:value-of select='*[local-name() = "responseType"]' />
urgency - <xsl:value-of select='*[local-name() = "urgency"]' />
severity - <xsl:value-of select='*[local-name() = "severity"]' />
certainty - <xsl:value-of select='*[local-name() = "certainty"]' />
audience - <xsl:value-of select='*[local-name() = "audience"]' />
<xsl:for-each select='*[local-name() = "eventCode"]' >
Value name - <xsl:value-of select='*[local-name() = "valueName"]' />
Value - <xsl:value-of select='*[local-name() = "value"]' />
</xsl:for-each>
effective - <xsl:value-of select='*[local-name() = "effective"]' />
onset - <xsl:value-of select='*[local-name() = "onset"]' />
expires - <xsl:value-of select='*[local-name() = "expires"]' />
senderName - <xsl:value-of select='*[local-name() = "senderName"]' />
headline - <xsl:value-of select='*[local-name() = "headline"]' />
description - <xsl:value-of select='*[local-name() = "description"]' />
instruction - <xsl:value-of select='*[local-name() = "instruction"]' />
web - <xsl:value-of select='*[local-name() = "web"]' />
contact - <xsl:value-of select='*[local-name() = "contact"]' />
<xsl:for-each select='*[local-name() = "parameter"]' >
Value name - <xsl:value-of select='*[local-name() = "valueName"]' />
Value - <xsl:value-of select='*[local-name() = "value"]' />
</xsl:for-each>
<xsl:for-each select='*[local-name() = "resource"]' >
resourceDesc - <xsl:value-of select='*[local-name() = "resourceDesc"]'/>
mimeType - <xsl:value-of select='*[local-name() = "mimeType"]'/>
size - <xsl:value-of select='*[local-name() = "size"]'/>
uri - <xsl:value-of select='*[local-name() = "uri"]'/>
derefUri - <xsl:value-of select='*[local-name() = "derefUri"]'/>
digest - <xsl:value-of select='*[local-name() = "digest"]'/>
</xsl:for-each>
<xsl:for-each select='*[local-name() = "area"]' >
areaDesc - <xsl:value-of select='*[local-name() = "areaDesc"]'/>
polygon - <xsl:value-of select='*[local-name() = "polygon"]'/>
circle - <xsl:value-of select='*[local-name() = "circle"]'/>
<xsl:for-each select='*[local-name() = "geocode"]' >
geocode - <xsl:value-of select='*[local-name() = "valueName"]'/>  <xsl:value-of select='*[local-name() = "value"]'/> 
</xsl:for-each>
altitude - <xsl:value-of select='*[local-name() = "altitude"]'/>
ceiling - <xsl:value-of select='*[local-name() = "ceiling"]'/>
</xsl:for-each>
</xsl:for-each>
	
								</HtH_Content>
								<HtH_Property>
									<HtH_PropertyKey>Subject</HtH_PropertyKey>
									<HtH_PropertyValue>CAP MESSAGE CONVERTED TO HUMAN_TO_HUMAN</HtH_PropertyValue>
								</HtH_Property>
								<HtH_Property>
									<HtH_PropertyKey>Format</HtH_PropertyKey>
									<HtH_PropertyValue>text/plain</HtH_PropertyValue>
								</HtH_Property>
							</HtH_Message>
						</embeddedXMLContent>
					</contentXML>
				</contentObject>
			</content>
		</edxlDistribution>
	</xsl:template>
</xsl:stylesheet>
