<?xml version='1.0' ?>
<!-- MS04 To AOL COC
 	 MS09 To AOL SOIR -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:b="urn:oasis:names:tc:emergency:cap:1.2"
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg" xmlns:c="urn:oasis:names:tc:emergency:EDXL:RM:1.0"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
	<xsl:template match="/">
		<xsl:variable name="sentDate" select="data:UTCDate()" />
		<xsl:variable name="bodyMessage" select="AOLMessage/Body" />
		<a:ResponseToRequestResource>
			<a:MessageID><xsl:value-of select="AOLMessage/ThreadID" />:<xsl:value-of select="AOLMessage/MessageID" /></a:MessageID>
			<a:SentDateTime><xsl:value-of select="$sentDate" /></a:SentDateTime>
			<a:MessageContentType>ResponseToRequestResource</a:MessageContentType>
			<a:MessageDescription><xsl:value-of select="AOLMessage/Subject" /></a:MessageDescription>
			<a:OriginatingMessageID><xsl:value-of select="AOLMessage/ThreadID" /></a:OriginatingMessageID>
			<a:PrecedingMessageID><xsl:value-of select="AOLMessage/ReplyID" /></a:PrecedingMessageID>
			<a:IncidentInformation>
				<c:IncidentDescription><xsl:value-of select="AOLMessage/Subject" /></c:IncidentDescription>
			</a:IncidentInformation>
			<a:ContactInformation>
				<c:ContactDescription><xsl:value-of select="AOLMessage/From/Name" /></c:ContactDescription>
				<c:ContactLocation><c:LocationDescription><xsl:value-of select="AOLMessage/Geometry" /></c:LocationDescription></c:ContactLocation>
			</a:ContactInformation>
			<a:ResourceInformation>
				<a:ResourceInfoElementID><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' TypeStructure ')" /></a:ResourceInfoElementID>
				<a:ResponseInformation>
					<c:PrecedingResourceInfoElementID><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' TypeStructure ')" /></c:PrecedingResourceInfoElementID>
					<c:ResponseType><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' ResponseType ')" /></c:ResponseType>
				</a:ResponseInformation>
			</a:ResourceInformation>
		</a:ResponseToRequestResource>
	</xsl:template>
</xsl:stylesheet>