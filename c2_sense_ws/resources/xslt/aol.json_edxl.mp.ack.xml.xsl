<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:b="urn:oasis:names:tc:emergency:cap:1.2"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data"
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:DE:2.0">
	<xsl:template match="/">
		<xsl:variable name="sentDate" select="data:UTCDate()" />
		<a:edxlDistribution xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns="urn:oasis:names:tc:emergency:EDXL:DE:2.0" xmlns:edxl-gsf="urn:oasis:names:tc:emergency:edxl:gsf:1.0"
			xmlns:ct="urn:oasis:names:tc:emergency:edxl:ct:1.0" xmlns:gml="http://www.opengis.net/gml/3.2"
			xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="extended">
			<a:distributionID>
				<xsl:value-of select="AOLMessage/ThreadID" />:<xsl:value-of select="AOLMessage/MessageID" />
			</a:distributionID>
			<a:senderID>
				<xsl:value-of select="AOLMessage/From/AOLNetworkID" />@<xsl:value-of select="AOLMessage/From/Name" />
			</a:senderID>
			<a:dateTimeSent>
				<xsl:value-of select="$sentDate" />
			</a:dateTimeSent>
			<a:dateTimeExpires>
				<xsl:value-of select="$sentDate" />
			</a:dateTimeExpires>
			<a:distributionStatus>Exercise</a:distributionStatus>
			<a:distributionKind>Ack</a:distributionKind>
			<a:content xlink:type="resource">
				<a:contentObject xlink:type="resource">
					<a:contentDescriptor>
						<a:contentDescription>
							AOL Message for acknowledgement
						</a:contentDescription>
					</a:contentDescriptor>
					<a:contentXML>
						<a:embeddedXMLContent>
							<AOLMessage xmlns="aol:1.1">
								<MessageID>
									<xsl:value-of select="AOLMessage/MessageID" />
								</MessageID>
								<ThreadID>
									<xsl:value-of select="AOLMessage/ThreadID" />
								</ThreadID>
								<ReplyID>
									<xsl:value-of select="AOLMessage/ReplyID" />
								</ReplyID>
								<From>
									<AOLNetworkID><xsl:value-of select="AOLMessage/From/AOLNetworkID" /></AOLNetworkID>
									<Name><xsl:value-of select="AOLMessage/From/Name" /></Name>
								</From>
								<xsl:for-each select="AOLMessage/To">
									<To>
										<AOLNetworkID><xsl:value-of select="AOLNetworkID" /></AOLNetworkID>
										<Name>
											<xsl:value-of select="/Name" />
										</Name>
									</To>
								</xsl:for-each>
								<xsl:for-each select="AOLMessage/CC">
									<CC>
										<AOLNetworkID><xsl:value-of select="AOLNetworkID" /></AOLNetworkID>
										<Name>
											<xsl:value-of select="/Name" />
										</Name>
									</CC>
								</xsl:for-each>
								<Subject><xsl:value-of select="AOLMessage/Subject" /></Subject>
								<Body><xsl:value-of select="AOLMessage/Body" /></Body>
							</AOLMessage>
						</a:embeddedXMLContent>
					</a:contentXML>
				</a:contentObject>
			</a:content>
		</a:edxlDistribution>
	</xsl:template>
</xsl:stylesheet>