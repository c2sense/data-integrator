<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!-- MS04 To AOL SOIR
 	 MS09 To AOL SOIR -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" />
    <xsl:template match="/">
	    <xsl:variable name="AolIds" select='*[local-name() = "RequestResource"]/*[local-name() = "MessageID"]' />
	    <xsl:variable name="AolContact" select='*[local-name() = "RequestResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "AdditionalContactInformation"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]'/>
        <AOLMessage>
			<MessageID><xsl:value-of select="data:splitString($AolIds, ':', 1)" /></MessageID>
			<ThreadID><xsl:value-of select="data:splitString($AolIds, ':', 0)" /></ThreadID>
			<From>
				<AOLNetworkID><xsl:value-of select="data:splitString($AolContact, ':', 0)" /></AOLNetworkID>
				<Name><xsl:value-of select="data:splitString($AolContact, ':', 1)" /></Name>
			</From>
			<To>
				<AOLNetworkID>TO</AOLNetworkID>
				<Name>TO</Name>
			</To>
			<Subject><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "IncidentInformation"]/*[local-name() = "IncidentDescription"]'/></Subject>
			<Body><![CDATA[<HTML><BODY><P><SPAN>== TypeStructure ==</SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ResourceInformation"]/*[local-name() = "ResourceInfoElementID"]' /><![CDATA[</SPAN></P><P><SPAN>== Description == </SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ResourceInformation"]/*[local-name() = "Resource"]/*[local-name() = "Description"]' /><![CDATA[</SPAN></P></BODY></HTML>]]></Body>
			<xsl:if test='*[local-name() = "RequestResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "ContactLocation"] != ""'>
			<Geometry><xsl:value-of select='*[local-name() = "RequestResource"]/*[local-name() = "ContactInformation"]/*[local-name() = "ContactLocation"]'/></Geometry>
			</xsl:if>
		</AOLMessage>
    </xsl:template>
</xsl:stylesheet>
