<?xml version='1.0' ?>
<!-- MS10 FireBrigade To AOL SOIR 
	 MS10 FireBrigade To AOL COC -->
<xsl:stylesheet version="1.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:b="urn:oasis:names:tc:emergency:cap:1.2"
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:SitRep:1.0" xmlns:d="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xnl"
	xmlns:c="urn:oasis:names:tc:emergency:edxl:ct:1.0"
	xmlns:e="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xal"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
	<xsl:template match="/">
		<xsl:variable name="aolId"><xsl:value-of select="data:generateAOLId()" /></xsl:variable>
		<a:sitRep>
			<a:messageID><xsl:value-of select="$aolId"/>:<xsl:value-of select="$aolId"/></a:messageID>
			<a:preparedBy>
				<c:personDetails>
					<d:personName>
						<d:nameElement>
							<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sender"]' />
						</d:nameElement>
					</d:personName>
				</c:personDetails>
				<c:timeValue>
					<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sent"]' />
				</c:timeValue>
			</a:preparedBy>
			<a:authorizedBy>
				<c:personDetails>
					<d:personName>
						<d:nameElement>
							<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sender"]' />
						</d:nameElement>
					</d:personName>
				</c:personDetails>
				<c:timeValue>
					<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sent"]' />
				</c:timeValue>
			</a:authorizedBy>
			<a:reportPurpose><xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "msgType"]'/> - <xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "info"]/*[local-name() = "event"]'/> <xsl:for-each select="b:alert/b:info/b:eventCode"><xsl:if test="b:valueName = 'Code_L2'"> - <xsl:value-of select="b:value" /></xsl:if></xsl:for-each></a:reportPurpose>
			<a:reportNumber>1</a:reportNumber>
			<a:reportVersion>Initial</a:reportVersion>
			<a:forTimePeriod>
				<c:fromDateTime>
					<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sent"]' />
				</c:fromDateTime>
				<c:toDateTime>
					<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sent"]' />
				</c:toDateTime>
			</a:forTimePeriod>
			<a:reportTitle>[MangementReport] Evento <xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "identifier"]' /></a:reportTitle>
			<a:incidentID>
				<xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "identifier"]' />
			</a:incidentID>
			<a:reportConfidence>Unsure</a:reportConfidence>
			<a:severity>Unknown</a:severity>
			<a:report xsi:type="a:ManagementReportingSummaryType">
				<a:situationSummary>
				<!-- 
				<a:incidentCause>
					<xsl:choose>
					    <xsl:when test='*[local-name() = "alert"]/*[local-name() = "info"]'>
					    	<xsl:for-each select='*[local-name() = "alert"]/*[local-name() = "info"]' >
					    		<xsl:for-each select='*[local-name() = "category"]' >
								category - <xsl:value-of select='.' />
								</xsl:for-each>
								event - <xsl:value-of select='*[local-name() = "event"]' />
					    	</xsl:for-each>
					    </xsl:when>
					    <xsl:otherwise>For more detailed info see cap message with ID: <xsl:value-of select="b:alert/b:identifier" /></xsl:otherwise>
					  </xsl:choose>
				</a:incidentCause>
				
				Modificato e seguo le indicazioni della mail di Biagio Re-definition of MS 10 di venerdì 14/04/2017 13:09
				
				V stato == status
				X same == missing
				X Messaggio == missing
				V prot == Param/REFNUM 
				V data
				V mittente
                V operatore
				V Note intervento
				
				-->
				<a:incidentCause> 
<xsl:for-each select="b:alert/b:info/b:parameter"><xsl:if test="b:valueName = 'REFNUM'"> Prot: <xsl:value-of select="b:value" /></xsl:if><xsl:if test="b:valueName = 'timecall'"> Data: <xsl:value-of select="b:value" /></xsl:if></xsl:for-each> Mittente: <xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "sender"]' />&lt;BR/&gt;
<!-- <xsl:for-each select="b:alert/b:info/b:parameter"><xsl:if test="b:valueName = 'INCIDENTPROGRESS'"> Stato: <xsl:value-of select="b:value" /></xsl:if></xsl:for-each>&lt;BR/&gt; -->
Stato: <xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "status"]' />&lt;BR/&gt;
Indirizzo: <xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "info"]/*[local-name() = "area"]/*[local-name() = "areaDesc"]' />&lt;BR/&gt;
Note Intervento: <xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "info"]/*[local-name() = "description"]' />&lt;BR/&gt;
Note operatore: <xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "info"]/*[local-name() = "instruction"]' />&lt;BR/&gt;
Operatore: <xsl:value-of select='*[local-name() = "alert"]/*[local-name() = "source"]' /></a:incidentCause>
				<a:significantEvents>MassNotifications</a:significantEvents>
				<a:damageAssessmentInformation></a:damageAssessmentInformation>
				<a:hazMatIncidentReport></a:hazMatIncidentReport>
				<a:extentOfContamination>
					<c:EDXLGeoPoliticalLocation>
						<c:address></c:address>
					</c:EDXLGeoPoliticalLocation>
				</a:extentOfContamination>
				<a:generalPopulationStatus></a:generalPopulationStatus>
				<a:externalAffairs></a:externalAffairs>
				<a:humanLifeAndSafetyThreat></a:humanLifeAndSafetyThreat>
				<a:lifeAndSafetyThreat>PotentialFutureThreat</a:lifeAndSafetyThreat>
				</a:situationSummary>
			</a:report>
		</a:sitRep>
	</xsl:template>
</xsl:stylesheet>