<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HtH_Message>
			<HtH_Content>
			
Threats: <xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Threats"]/*[local-name() = "Description"]'/>
Assets: <xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Assets"]/*[local-name() = "Description"]'/>
Execution: <xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Execution"]/*[local-name() = "ConceptOfOperations"]'/>
CommandAndSignal: <xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "CommandAndSignal"]/*[local-name() = "Signaling"]'/>

	</HtH_Content>
	<HtH_Property>
		<HtH_PropertyKey>Subject</HtH_PropertyKey>
		<HtH_PropertyValue><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Mission"]/*[local-name() = "Summary"]'/></HtH_PropertyValue>
	</HtH_Property>
	<HtH_Property>
		<HtH_PropertyKey>Format</HtH_PropertyKey>
		<HtH_PropertyValue>text/plain</HtH_PropertyValue>
	</HtH_Property>
</HtH_Message>
	</xsl:template>
</xsl:stylesheet>
