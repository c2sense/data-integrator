<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:b="urn:oasis:names:tc:emergency:cap:1.2"
	xmlns:a="urn:oasis:names:tc:emergency:EDXL:RM:1.0:msg" xmlns:c="urn:oasis:names:tc:emergency:EDXL:RM:1.0"
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
	<xsl:template match="/">
		<!--
		<xsl:variable name="thread_id" select="b:alert/b:incidents" />
		<xsl:variable name="AolIds" select="data:splitString($incidents, ',', 1)" />
		Integrazione con FireBrigade
		-->
		<xsl:variable name="bodyMessage" select="b:alert/b:info/b:instruction" />
		<xsl:variable name="AolIds"><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' MessageID ')" /></xsl:variable>
		<a:ResponseToRequestResource>
			<a:MessageID><xsl:value-of select="data:splitString($AolIds, ':', 0)" />:<xsl:value-of select="data:generateAOLId()" /></a:MessageID>
			<a:SentDateTime><xsl:value-of select="b:alert/b:sent" /></a:SentDateTime>
			<a:MessageContentType>ResponseToRequestResource</a:MessageContentType>
			<a:MessageDescription>
				<xsl:for-each select="b:alert/b:info/b:parameter">
					<xsl:if test="b:valueName = 'Resource'">
						Richiesta pervenuta: <xsl:value-of select="b:value" />
					</xsl:if>
					<xsl:if test="b:valueName = 'Response'">
						Risposta: <xsl:value-of select="data:trimString(b:value)" />
					</xsl:if>
				</xsl:for-each>
				<xsl:if test="b:alert/b:status != ''">
					Status: <xsl:value-of select="b:alert/b:status" /> -
				</xsl:if>
				<xsl:if test="b:alert/b:msgType != ''">
					MsgType: <xsl:value-of select="b:alert/b:msgType" /> -  
				</xsl:if>
				<xsl:if test="b:alert/b:source != ''">
					Source: <xsl:value-of select="b:alert/b:source" /> - 
				</xsl:if>
				<xsl:if test="b:alert/b:scope != ''">
					Scope: <xsl:value-of select="b:alert/b:scope" /> - 
				</xsl:if>
				<xsl:if test="b:alert/b:note != ''">
					Note: <xsl:value-of select="b:alert/b:note" />
				</xsl:if>
			</a:MessageDescription>
			<a:OriginatingMessageID><xsl:value-of select="data:splitString($AolIds, ':', 0)" /></a:OriginatingMessageID>
			<a:PrecedingMessageID><xsl:value-of select="data:splitString($AolIds, ':', 1)" /></a:PrecedingMessageID>
			<a:IncidentInformation>
				<c:IncidentDescription>[ResponseToRequestResource] R: <xsl:value-of select="b:alert/b:info/b:event" /></c:IncidentDescription>
			</a:IncidentInformation>
			<a:ContactInformation>
				<c:ContactDescription>
					<xsl:value-of select="b:alert/b:sender" />
				</c:ContactDescription>
			</a:ContactInformation>
			<!-- 
			<a:ResourceInformation>
				<xsl:variable name="resourceInfoElementID" >
					<xsl:for-each select="b:alert/b:info/b:parameter">
						<xsl:if test="b:valueName = 'ResourceInfoElementID'">
							<xsl:value-of select="b:value"/>
						</xsl:if>
					</xsl:for-each>
				</xsl:variable>
				<xsl:for-each select="b:alert/b:info/b:parameter">
					<xsl:if test="b:valueName = 'ResourceInfoElementID'">
						<a:ResourceInfoElementID><xsl:value-of select="b:value" /></a:ResourceInfoElementID>
					</xsl:if>
					<xsl:if test="b:valueName = 'Response'">
						<a:ResponseInformation>
							<c:PrecedingResourceInfoElementID><xsl:value-of select="$resourceInfoElementID" /></c:PrecedingResourceInfoElementID>
							<c:ResponseType><xsl:value-of select="data:trimString(b:value)" /></c:ResponseType>
						</a:ResponseInformation>
					</xsl:if>
				</xsl:for-each>
			</a:ResourceInformation>
			Integrazione con FireBrigade
			-->
			<a:ResourceInformation>
				<a:ResourceInfoElementID><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' TypeStructure ')" /></a:ResourceInfoElementID>
				<a:ResponseInformation>
					<c:PrecedingResourceInfoElementID><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' TypeStructure ')" /></c:PrecedingResourceInfoElementID>
					<c:ResponseType><xsl:value-of select="data:getValueFromAolBodyMap($bodyMessage, ' ResponseType ')" /></c:ResponseType>
				</a:ResponseInformation>
			</a:ResourceInformation>
		</a:ResponseToRequestResource>
	</xsl:template>
</xsl:stylesheet>