<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!-- MS04 To AOL COC
 	 MS09 To AOL SOIR 
 	 MS10 FireBrigade To AOL SOIR COC-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" />
    <xsl:template match="/">
	    <xsl:variable name="AolIds" select='*[local-name() = "sitRep"]/*[local-name() = "messageID"]' />
        <AOLMessage>
			<MessageID><xsl:value-of select="data:splitString($AolIds, ':', 1)" /></MessageID>
			<ThreadID><xsl:value-of select="data:splitString($AolIds, ':', 0)" /></ThreadID>
			<xsl:if test="data:splitString($AolIds, ':', 2) !=''">
			<ReplyID><xsl:value-of select="data:splitString($AolIds, ':', 2)" /></ReplyID>
			</xsl:if>
			<From>
				<AOLNetworkID><xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "preparedBy"]/*[local-name() = "personDetails"]/*[local-name() = "personName"]/*[local-name() = "nameElement"]'/></AOLNetworkID>
				<Name><xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "preparedBy"]/*[local-name() = "personDetails"]/*[local-name() = "personName"]/*[local-name() = "nameElement"]'/></Name>
			</From>
			<To>
				<AOLNetworkID>TO</AOLNetworkID>
				<Name>TO</Name>
			</To>
			<Subject><xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "reportTitle"]'/></Subject>
			<Body><![CDATA[<HTML><BODY><P><SPAN>== ReportPurpose ==</SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "reportPurpose"]' /><![CDATA[</SPAN></P><P><SPAN>== SituationSummary == </SPAN></P><P><SPAN>]]><xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "report"]/*[local-name() = "situationSummary"]/*[local-name() = "incidentCause"]' /><![CDATA[</SPAN></P></BODY></HTML>]]></Body>
			<xsl:if test='*[local-name() = "sitRep"]/*[local-name() = "report"]/*[local-name() = "situationSummary"]/*[local-name() = "damageAssessmentInformation"] != ""' >
			<Geometry><xsl:value-of select='*[local-name() = "sitRep"]/*[local-name() = "report"]/*[local-name() = "situationSummary"]/*[local-name() = "damageAssessmentInformation"]' /></Geometry>
			</xsl:if>
		</AOLMessage>
    </xsl:template>
</xsl:stylesheet>
