<?xml version='1.0' ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:b="http://www.c2sense.eu/edxl-mp"
	xmlns="urn:oasis:names:tc:emergency:cap:1.2" xmlns:c="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xnl"
	xmlns:d="urn:oasis:names:tc:emergency:edxl:ct:1.0" xmlns:e="http://www.opengis.net/gml/3.2"
	xmlns:f="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xal" xmlns:g="urn:oasis:names:tc:emergency:edxl:ciq:1.0:xpil"
	xmlns:data="xalan://util.C2SDIUtils">
	<xsl:template match="/">
		<alert>
			<identifier><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "MessageID"]' /></identifier>
			<!-- 
			<xsl:if test='*[local-name() = "MissionPlan"]/*[local-name() = "PreparedBy"]/*[local-name() = "PersonDetails"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]'>
				<sender><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "PreparedBy"]/*[local-name() = "PersonDetails"]/*[local-name() = "ElectronicAddressIdentifiers"]/*[local-name() = "ElectronicAddressIdentifier"]' /></sender>
			</xsl:if>
			According to what was agreed with Daniel from Safran change this to dummySender in order to be replaced by LAR
			-->
			<sender>dummySender innovapuglia@pec.rupar.puglia.it</sender>
			<sent><xsl:value-of select="data:UTCDate()" /></sent>
			<status>System</status>
			<msgType>Alert</msgType>
			<source>DI conversion: edxl mission plan to cap alert</source>
			<scope>Private</scope>
			<!--
			<addresses><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoPoliticalLocation"]/*[local-name() = "Address"]/*[local-name() = "FreeTextAddress"]/*[local-name() = "AddressLine"]' /></addresses>
			According to what was agreed with Daniel from Safran change this to dummyAddresses in order to be replaced by LAR
			-->
			<addresses>dummyAddresses</addresses>
			<info>
				<language>it-IT</language>
				<category>Other</category>
				<event>From Mission Plan message the observed/known situation THREATS are: <xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Threats"]/*[local-name() = "Description"]' /> and the observed/known situation ASSETS are: <xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Assets"]/*[local-name() = "Description"]' /></event>
				<urgency>Unknown</urgency>
				<severity>Unknown</severity>
				<certainty>Unknown</certainty>
				<senderName><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "PreparedBy"]/*[local-name() = "PersonDetails"]/*[local-name() = "PersonName"]/*[local-name() = "NameElement"]' /></senderName>
				<headline><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Mission"]/*[local-name() = "Summary"]' /></headline>
				<description><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Execution"]/*[local-name() = "ConceptOfOperations"]' /></description>
				<instruction><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "CommandAndSignal"]/*[local-name() = "Signaling"]' /></instruction>
				<contact>Authorized by: <xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "AuthorizedBy"]/*[local-name() = "PersonDetails"]/*[local-name() = "PersonName"]/*[local-name() = "NameElement"]' /></contact>
				<xsl:if test='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "Envelope"]'>
					<area>
						<areaDesc>Mission Plan Envelope</areaDesc>
						<geocode>
							<valueName>Envelope lowerCorner</valueName>
							<value><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "Envelope"]/*[local-name() = "lowerCorner"]' /></value>
						</geocode>
						<geocode>
							<valueName>Envelope upperCorner</valueName>
							<value><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "Envelope"]/*[local-name() = "upperCorner"]' /></value>
						</geocode>
					</area>
				</xsl:if>
				<xsl:if test='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "LineString"]'>
					<area>
						<areaDesc>Mission Plan LinearString</areaDesc>
						<geocode>
							<valueName>LineString pos</valueName>
							<value><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "LineString"]/*[local-name() = "pos"]' /></value>
						</geocode>
						<geocode>
							<valueName>Envelope posList</valueName>
							<value><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "LineString"]/*[local-name() = "posList"]' /></value>
						</geocode>
					</area>
				</xsl:if>
				<xsl:if test='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "Point"]'>
					<area>
						<areaDesc>Mission Plan Point</areaDesc>
						<geocode>
							<valueName>Point</valueName>
							<value><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "Point"]' /></value>
						</geocode>
					</area>
				</xsl:if>
				<xsl:if test='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "CircleByCenterPoint"]'>
					<area>
						<areaDesc>Mission Plan Circle</areaDesc>
						<circle><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "CircleByCenterPoint"]/*[local-name() = "pos"]' />
							<xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "CircleByCenterPoint"]/*[local-name() = "radius"]' /></circle>
					</area>
				</xsl:if>
				<xsl:if test='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "Polygon"]'>
					<area>
						<areaDesc>Mission Plan Polygon</areaDesc>
						<polygon><xsl:value-of select='*[local-name() = "MissionPlan"]/*[local-name() = "Situation"]/*[local-name() = "Field"]/*[local-name() = "EDXLGeoLocation"]/*[local-name() = "Polygon"]/*[local-name() = "exterior"]/*[local-name() = "LinearRing"]' /></polygon>
					</area>
				</xsl:if>
			</info>
		</alert>
	</xsl:template>
</xsl:stylesheet>