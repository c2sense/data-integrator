<?xml version='1.0' ?>
<!-- MS04 To AOL SOIR
 	 MS09 To AOL SOIR NEED TO CHECK THESE MAY TOO MUCH COPY AND PASTE! -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:data="xalan://util.C2SDIUtils" extension-element-prefixes="data">
	<xsl:template match="/">
		<xsl:variable name="aolId"><xsl:value-of select="data:generateAOLId()" /></xsl:variable>
		<AOLMessage>
			<MessageID><xsl:value-of select="$aolId" /></MessageID>
			<ThreadID><xsl:value-of select="$aolId" /></ThreadID>
			<!-- 
				New transformation for MS
				I don't think we will use this
			<xsl:if test='*[local-name() = "ResponseToRequestResource"]/*[local-name() = "PrecedingMessageID"] != ""'>
				<ReplyID><xsl:value-of select='*[local-name() = "ResponseToRequestResource"]/*[local-name() = "PrecedingMessageID"]'/></ReplyID>
			</xsl:if>
			 -->
			<From>
				<AOLNetworkID>FROM</AOLNetworkID>
				<Name>FROM</Name>
			</From>
			<To>
				<AOLNetworkID>TO</AOLNetworkID>
				<Name>TO</Name>
			</To>
			<Subject><xsl:for-each select="HtH_Message/HtH_Property"><xsl:if test="HtH_PropertyKey = 'Subject'">[ResponseToRequestResource] <xsl:value-of select="HtH_PropertyValue"/></xsl:if></xsl:for-each></Subject>
			<Body><xsl:value-of select='*[local-name() = "HtH_Message"]/*[local-name() = "HtH_Content"]'/></Body>
		</AOLMessage>
	</xsl:template>
</xsl:stylesheet>